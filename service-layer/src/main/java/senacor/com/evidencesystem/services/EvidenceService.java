package senacor.com.evidencesystem.services;

import org.json.JSONArray;
import org.json.JSONObject;
import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.dao.ProfileDao;
import senacor.com.evidencesystem.dao.ProjectDao;
import senacor.com.evidencesystem.dao.RecordDao;
import senacor.com.evidencesystem.dao.UserDao;
import senacor.com.evidencesystem.exception.ProjectNameAlreadyExistsException;
import senacor.com.evidencesystem.exception.RecordFromFutureException;
import senacor.com.evidencesystem.exception.RecordFromPastException;
import senacor.com.evidencesystem.exception.UserAlreadyExistsException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Evidence service
 * Created by fjarab on 05.08.2016.
 */
@Stateless
public class EvidenceService implements Serializable {

    @Inject
    private ProfileDao profileDao;

    @Inject
    private RecordDao recordDao;

    @Inject
    private ProjectDao projectDao;

    @Inject
    private UserDao userDao;

    //Profile services
    public void insertEntity(Profile profile) {
        profileDao.insertEntity(profile);
    }

    public void deleteEntity(Profile entity) {
        profileDao.deleteEntity(entity);
    }

    public Long getProfilesCount() {
        return profileDao.getCount();
    }

    public Profile getProfileByUser(User user) {
        return profileDao.getProfileByUser(user);
    }

    //Project services
    public List<Project> getAllProjects() {
        return projectDao.getAllProjects();
    }

    public void insertEntity(Project entity) {
        projectDao.insertEntity(entity);
    }

    public void deleteEntity(Project entity) {
        projectDao.deleteEntity(entity);
    }

    public Long getProjectsCount() {
        return projectDao.getCount();
    }

    public Project getProjectByUser(User user) {
        return projectDao.getProjectByUser(user);
    }

    public void updateProjectName(Project project, String newName) throws ProjectNameAlreadyExistsException {
        projectDao.updateProjectName(project, newName);
    }

    //Record services
    public void insertEntity(Record record) {
        recordDao.insertEntity(record);
    }

    public void deleteEntity(Record entity) {
        recordDao.deleteEntity(entity);
    }

    public Long getRecordsCount() {
        return recordDao.getCount();
    }

    public List<Record> getRecordsByCurrentMonth(User user, Date date) {
        return recordDao.getRecordsByCurrentMonth(user, date);
    }

    public List<Record> getRecordsByMultipleProperties(List<String> emails, List<String> projectNames,
                                                       List<Date> months, List<String> years, List<Boolean> states,
                                                       List<Boolean> locks) {
        return recordDao.getRecordsByMultipleProperties(emails, projectNames, months, years, states, locks);
    }

    public void confirmRecords(List<Record> records) {
        recordDao.confirmRecords(records);
    }

    public void unconfirmRecords(List<Record> records) {
        recordDao.unconfirmRecords(records);
    }

    public void lockRecords(List<Record> records) throws RecordFromFutureException, RecordFromPastException {
        recordDao.lockRecords(records);
    }

    public void unlockRecords(List<Record> records) {
        recordDao.unlockRecords(records);
    }

    public Float calculateTotalHoursForOneRecord(Record record) {
        return recordDao.calculateTotalHoursForOneRecord(record);
    }

    public Float calculateTotalHours(User user, Date date) {
        return recordDao.calculateTotalHours(user, date);
    }

    public List<Record> getAllRecords() {
        return recordDao.getAllRecords();
    }

    public void updateRecordsPauseDuration(Record record, Date durationOfPause) {
        recordDao.updateRecordsPauseDuration(record, durationOfPause);
    }

    public void updateRecordsTimeOfStart(Record record, Date timeOfStart) {
        recordDao.updateRecordsTimeOfStart(record, timeOfStart);
    }

    public void updateRecordsTimeOfEnd(Record record, Date timeOfEnd) {
        recordDao.updateRecordsTimeOfEnd(record, timeOfEnd);
    }

    public void updateRecordsTypeOfWork(Record record, String typeOfWork) {
        recordDao.updateRecordsTypeOfWork(record, typeOfWork);
    }

    //User services
    public void insertEntity(User user) throws UserAlreadyExistsException {
        userDao.insertEntity(user);
    }

    public void deleteEntity(User user) {
        userDao.deleteEntity(user);
    }

    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public Long getUsersCount() {
        return userDao.getCount();
    }

    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    public void setUsersProfile(User user, Profile profile) {
        userDao.setUsersProfile(user, profile);
    }

    public void setUsersProject(User user, Project project) {
        userDao.setUsersProject(user, project);
    }

}
