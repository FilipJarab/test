package senacor.com.evidencesystem.beans;

import org.primefaces.event.SelectEvent;
import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.api.enumeration.TypeOfContract;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class represents the managed bean for Manage Profile page. User (Student) can update his data.
 * The xhtml file for this managed bean is the manage_profile.xhtml file.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 31.05.2016.
 */

@Named
@RequestScoped
public class ManageProfileModelView implements Serializable {
    private SimpleDateFormat dateFormat;
    private String actualProjectName;
    private List<String> projectsNames;
    private String userName;
    private User user;
    private Date startDate;
    private Date endDate;
    private List<Project> projects;
    private TypeOfContract contractType;
    private Profile profile;
    private String emptyFieldsMessage;
    private String successfulDataUpdateMessage;
    private String emptyFieldsInf;
    private String successfulDataUpdateInf;
    private String startDateIsGreater;
    private String errorMessage;

    @Inject
    private EvidenceService evidenceService;

    @Inject
    LoginBean loginBean;

    @PostConstruct
    public void create() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)
                context.getExternalContext().getRequest();
        String usersEmail = request.getUserPrincipal().toString();

        dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        projectsNames = new ArrayList<>();
        user = evidenceService.getUserByEmail(usersEmail);
        userName = user.getFirstName() + " " + user.getLastName();
        loadMessages();
        loadDateFromDB();
    }

    private void loadMessages() {
        Locale locale = loginBean.getLocale();
        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        emptyFieldsMessage = messages.getString("emptyMultipleFieldsMessage");
        successfulDataUpdateMessage = messages.getString("successfulDataUpdateMessage");
        emptyFieldsInf = messages.getString("emptyFieldsInf");
        successfulDataUpdateInf = messages.getString("successfulDataUpdateInf");
        startDateIsGreater = messages.getString("startDateIsGreater");
        errorMessage = messages.getString("errorMessage");
    }

    public void applySettings() {
        if (!startDate.before(endDate)) {
            FacesContext.getCurrentInstance().addMessage("wrong_date", new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, startDateIsGreater));
            return;
        }
        if (actualProjectName == null || contractType == null || startDate == null || endDate == null) {
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    emptyFieldsMessage, emptyFieldsInf);
            FacesContext.getCurrentInstance().addMessage("emptyFieldsManageProfilePage", facesMsg);
            return;
        } else {
            Project selectedProject = null;
            if (profile.isNull()) {
                profile = new Profile();
            }
            for (Project project : projects) {
                if (project.getProjectName().equals(actualProjectName))
                    selectedProject = project;
                profile.setProject(selectedProject);
            }
            profile.setTypeOfContract(contractType);
            profile.setDateOfStart(startDate);
            profile.setDateOfEnd(endDate);
            evidenceService.setUsersProfile(user, profile);
            evidenceService.setUsersProject(user, selectedProject);
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    successfulDataUpdateMessage, successfulDataUpdateInf);
            FacesContext.getCurrentInstance().addMessage("successUpdateManageProfilePage", facesMsg);
        }
    }

    public void changeStartDate(SelectEvent event) {
        Date date = (Date) event.getObject();
        startDate = date;
    }

    public void changeEndDate(SelectEvent event) {
        Date date = (Date) event.getObject();
        endDate = date;
    }

    private void loadDateFromDB() {
        profile = evidenceService.getProfileByUser(user);
        Project usersProjectFromDB = evidenceService.getProjectByUser(user);
        if (!(usersProjectFromDB.isNull())) {
            actualProjectName = usersProjectFromDB.getProjectName();
        }
        projects = evidenceService.getAllProjects();
        for (Project project : projects) {
            projectsNames.add(project.getProjectName());
        }
        if (!(profile.isNull())) {
            setContractType(profile.getTypeOfContract());
            startDate = profile.getDateOfStart();
            endDate = profile.getDateOfEnd();
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getProjectsNames() {
        return projectsNames;
    }

    public void setProjectsNames(List<String> projectsNames) {
        this.projectsNames = projectsNames;
    }

    public String getActualProjectName() {
        return actualProjectName;
    }

    public void setActualProjectName(String actualProjectName) {
        this.actualProjectName = actualProjectName;
    }

    public TypeOfContract getContractType() {
        return contractType;
    }

    public void setContractType(TypeOfContract contractType) {
        this.contractType = contractType;
    }

    public TypeOfContract[] getTypeOfContracts() {
        return TypeOfContract.values();
    }
}
