package senacor.com.evidencesystem.beans;

import com.google.common.hash.Hashing;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.UserAlreadyExistsException;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * This class represents the managed bean for Register Page. New users can execute the registration.
 * The xhtml file for this managed bean is the register_xhtml file.
 * Created by dbozo on 30.05.2016.
 */
@SessionScoped
@Named
public class RegisterPageModelView implements Serializable {
    private final String successfulMessage;
    private String emailAddress;
    private String password;
    private String confirmPassword;
    private String enterPasswordMessage;
    private String emptyFieldsMessage;
    private String differentPasswordsMessage;
    private String incorrectPasswordsMessage;
    private String checkDataMessage;
    private String alreadyRegisteredMessage;
    private String registrationMessage;
    private String congratulationMessage;
    private String successfullyRegisteredMessage;
    private String successfulRegistrationMessage;
    private final String SPACE = " ";

    @Inject
    private EvidenceService evidenceService;

    @Inject
    private LoginBean loginBen;

    public RegisterPageModelView() {
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        enterPasswordMessage = messages.getString("enterPasswordMessage");
        emptyFieldsMessage = messages.getString("emptyFieldsMessage");
        differentPasswordsMessage = messages.getString("differentPasswordsMessage");
        incorrectPasswordsMessage = messages.getString("incorrectPasswordsMessage");
        checkDataMessage = messages.getString("checkDataMessage");
        alreadyRegisteredMessage = messages.getString("alreadyRegisteredMessage");
        registrationMessage = messages.getString("registrationMessage");
        successfullyRegisteredMessage = messages.getString("successfullyRegisteredMessage");
        successfulRegistrationMessage = messages.getString("successfulRegistrationMessage");
        congratulationMessage = messages.getString("congratulationMessage");
        successfulMessage = messages.getString("successfulMessage");
    }

    public boolean isPasswordsEqual() {
        if (confirmPassword == null || password == null) {
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    enterPasswordMessage, emptyFieldsMessage);
            FacesContext.getCurrentInstance().addMessage(null, facesMsg);
            return false;
        }
        return confirmPassword.equals(password);
    }

    public void registerNewUser() {
        if (emailAddress.equals(EMPTY) || password.equals(EMPTY) || confirmPassword.equals(EMPTY)) {
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    checkDataMessage, emptyFieldsMessage);
            FacesContext.getCurrentInstance().addMessage("emptyFields", facesMsg);
            return;
        } else if (!isPasswordsEqual()) {
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    differentPasswordsMessage, incorrectPasswordsMessage);
            FacesContext.getCurrentInstance().addMessage("differentPassword", facesMsg);
            password = EMPTY;
            confirmPassword = EMPTY;
            return;
        }
        User user = new User(emailAddress, sha512(password));
        try {
            evidenceService.insertEntity(user);
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    congratulationMessage + SPACE + user.getFirstName() + SPACE + successfullyRegisteredMessage, successfulRegistrationMessage);
            FacesContext.getCurrentInstance().addMessage("correctRegistration", facesMsg);

        } catch (UserAlreadyExistsException e) {
            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    alreadyRegisteredMessage, registrationMessage);
            FacesContext.getCurrentInstance().addMessage("invalidRegistration", facesMsg);
            return;
        }
    }

    public String sha512(String passwordToHash){
        final String hashed = Hashing.sha512()
                .hashString(passwordToHash, StandardCharsets.UTF_8)
                .toString();
        return hashed;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
