package senacor.com.evidencesystem.beans;


import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

/**
 * Created by fjarab on 25.05.2016.
 * Backing bean for
 */

@Named
@SessionScoped
public class LoginBean implements Serializable {
    private String email;
    private String password;
    private Locale locale;

    public LoginBean() {
        locale = new Locale("en");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)
                context.getExternalContext().getRequest();
        String pageToReturn;
        try {
            request.login(email, this.password);
            //TODO: delete sout, create interceptor class for logging
            System.out.println(request.isUserInRole("ADMIN"));
        } catch (ServletException e) {
            e.printStackTrace();
            return "error_page.xhtml";
        }
        if (request.isUserInRole("ADMIN"))
            pageToReturn = "/office_manager_main_page.xhtml?faces-redirect=true";
        else
            pageToReturn = "/student_main_page.xhtml?faces-redirect=true";
        return pageToReturn;
    }

    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletRequest request = (HttpServletRequest)
                context.getExternalContext().getRequest();
        try {

            request.logout();
        } catch (ServletException e) {
            context.addMessage(null, new FacesMessage("Logout failed."));
        }
        return "/public/login?faces-redirect=true";
    }

    public void changeLanguage(String lang) {
        switch (lang) {
            case "sk":
                locale = new Locale("sk");
                break;
            case "en":
                locale = new Locale("en");
                break;
            case "de":
                locale = new Locale("de");
                break;
        }
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}



