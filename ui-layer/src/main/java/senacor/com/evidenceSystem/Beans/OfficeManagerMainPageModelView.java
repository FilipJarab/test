package senacor.com.evidencesystem.beans;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.primefaces.component.selectmanymenu.SelectManyMenuRenderer;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.api.enumeration.Rank;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;

/**
 * This class represents the managed bean for Office Manager Main Page. Office manager can check work time of employees,
 * filter it, then export it to csv file.
 * The xhtml file for this managed bean is the office_manager_main_page.xhtml.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 15.06.2016.
 */

@Named
@ViewScoped
public class OfficeManagerMainPageModelView extends SelectManyMenuRenderer implements Serializable {

    private static final int INIT_YEAR = 2015;
    public static final String EXPORT_TO_EXCEL_BUTTON_ID = "main_data_table_form:main_data_table:export_to_excel_button";
    private final String DATA_TABLE_ID = "main_data_table_form:main_data_table";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY");
    private String loggedUsersName;
    private List<String> projectsNameList;
    private List<String> monthsList;
    private List<String> yearsList;
    private List<String> typesOfData;
    private List<String> typesOfFinalization;
    private List<User> users;
    private List<User> selectedUsers;
    private List<Record> recordsList;
    private List<Record> confirmedRecords;
    private List<String> selectedNames;
    private List<String> selectedProjects;
    private List<String> selectedMonths;
    private List<String> selectedYears;
    private List<String> selectedDataTypes;
    private List<String> selectedTypeOfFinalization;
    private String errorMessage;
    private String successfulMessage;
    private String recordsConfirmedMessage;
    private String selectRecordMessage;
    private String notAvailableConfirmationMessage;
    private String finalText;
    private String notFinalText;
    private String confirmedText;
    private String unConfirmedText;
    private boolean recordsSelected;
    private int rowsPerPage;
    private List<String> paginatorList;
    private boolean paginator = true;

    @Inject
    private EvidenceService evidenceService;

    @Inject
    private LoginBean loginBean;

    @PostConstruct
    public void create() {
        setUpPaginator();
        loadMessages();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)
                facesContext.getExternalContext().getRequest();

        String usersEmail = request.getUserPrincipal().toString();
        User loggedUser = evidenceService.getUserByEmail(usersEmail);
        loggedUsersName = loggedUser.getFirstName() + " " + loggedUser.getLastName();
        selectedTypeOfFinalization = new ArrayList<>();
        recordsList = new ArrayList<>();
        selectedProjects = new ArrayList<>();
        selectedMonths = new ArrayList<>();
        selectedYears = new ArrayList<>();
        selectedDataTypes = new ArrayList<>();
        selectedNames = new ArrayList<>();
        confirmedRecords = new ArrayList<>();
        loadDataFromDB();
    }

    public void showResults() {
        Calendar calendar = Calendar.getInstance();
        confirmedRecords = new ArrayList<>();
        ArrayList<String> selectedEmails = new ArrayList<>();
        List<Boolean> selectedTypes = new ArrayList<>();
        List<Boolean> selectedTypesOfFinalisation = new ArrayList<>();
        List<Date> selectedMonths = new ArrayList<>();
        List<String> selectedYears = new ArrayList<>();
        List<String> selectedProjects = new ArrayList<>();
        SimpleDateFormat yearDateFormat = new SimpleDateFormat("YYYY");
        SimpleDateFormat monthDateFormat = new SimpleDateFormat("MMM", Locale.ENGLISH);

        for (User user : selectedUsers) {
            selectedEmails.add(user.getEmail());
        }
        for (String dataType : selectedDataTypes) {
            if (dataType.equals(confirmedText))
                selectedTypes.add(Boolean.TRUE);
            else if (dataType.equals(unConfirmedText))
                selectedTypes.add(Boolean.FALSE);
        }
        for (String finalisationType : selectedTypeOfFinalization) {
            if (finalisationType.equals(finalText))
                selectedTypesOfFinalisation.add(Boolean.TRUE);
            else if (finalisationType.equals(notFinalText))
                selectedTypesOfFinalisation.add(Boolean.FALSE);
        }
        for (String month : this.selectedMonths) {
            try {
                calendar.setTime(monthDateFormat.parse(month));
                selectedMonths.add(monthDateFormat.parse(month));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        for (String project : this.selectedProjects) {
            selectedProjects.add(project);
        }
        recordsList = evidenceService.getRecordsByMultipleProperties(selectedEmails, selectedProjects, selectedMonths,
                this.selectedYears, selectedTypes, selectedTypesOfFinalisation);
        Collections.sort(recordsList, (o1, o2) -> o1.getDate().compareTo(o2.getDate()));
        for (Record loadedRecord : recordsList) {
            if (loadedRecord.isAuthorized()) {
                confirmedRecords.add(loadedRecord);
            }
        }
        recordsSelected = !confirmedRecords.isEmpty();
        refreshTable();
    }

    public void confirmSelectedRecords() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        List<Record> unconfirmedRecords = new ArrayList<>();
        if (recordsList.isEmpty()) {
            facesContext.addMessage(errorMessage, new FacesMessage(selectRecordMessage));
            return;
        }
        if (confirmedRecords.isEmpty()) {
            evidenceService.unconfirmRecords(recordsList);
            facesContext.addMessage(null, new FacesMessage(recordsConfirmedMessage));
            return;
        }
        for (Record record : recordsList) {
            for (Record confirmedRecord : confirmedRecords) {
                if (!record.equals(confirmedRecord) && record.isAuthorized()) {
                    unconfirmedRecords.add(record);
                }
            }
        }
        if (!unconfirmedRecords.isEmpty()) {
            evidenceService.unconfirmRecords(unconfirmedRecords);
        }
        ArrayList<Record> checkedRecordsForConfirmation = new ArrayList<>();
        for (Record record : confirmedRecords) {
            if (record.isLocked())
                checkedRecordsForConfirmation.add(record);
        }
        if (checkedRecordsForConfirmation.isEmpty()) {
            facesContext.addMessage(errorMessage, new FacesMessage(notAvailableConfirmationMessage));
            return;
        }
        evidenceService.confirmRecords(checkedRecordsForConfirmation);
        facesContext.addMessage(null, new FacesMessage(successfulMessage));
        showResults();
    }

    public float calculateTotalTimeForRecord(Record record) {
        return evidenceService.calculateTotalHoursForOneRecord(record);
    }

    public void unlockRecords() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (recordsList.isEmpty() || confirmedRecords.isEmpty()) {
            facesContext.addMessage(null, new FacesMessage(errorMessage, selectRecordMessage));
            showResults();
            return;
        }
        evidenceService.unlockRecords(confirmedRecords);
        showResults();
    }

    public void enableExcelExport(SelectEvent event) {
        recordsSelected = true;
        refreshExportToExcelButton();
    }

    public void disableExcelExport(UnselectEvent event) {
        if (confirmedRecords.isEmpty()) {
            recordsSelected = false;
            refreshExportToExcelButton();
        }
    }

    public void changePagination(ValueChangeEvent event) {
        switch (event.getNewValue().toString()) {
            case "10":
                rowsPerPage = 10;
                paginator = true;
                break;
            case "25":
                rowsPerPage = 25;
                paginator = true;
                break;
            case "All": {
                rowsPerPage = 0;
                paginator = false;
                break;
            }
        }
        refreshTable();
    }

    public void toggleCheckBoxEvent(ToggleSelectEvent event) {
        recordsSelected = !confirmedRecords.isEmpty();
        refreshExportToExcelButton();
    }

    private void loadDataFromDB() {
        users = evidenceService.getAllUsers();
        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {
            User user = iterator.next();
            if (user.getRank().equals(Rank.ADMIN))
                iterator.remove();
        }
        List<Project> projects = evidenceService.getAllProjects();
        projectsNameList = new ArrayList<>();
        yearsList = new ArrayList<>();
        selectedUsers = new ArrayList<>();
        typesOfFinalization = asList(finalText, notFinalText);
        monthsList = asList("January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December");
        typesOfData = asList(confirmedText, unConfirmedText);

        for (Project project : projects) {
            projectsNameList.add(project.getProjectName());
        }
        int actualYear = Integer.parseInt(simpleDateFormat.format(new Date()));
        int initYear = INIT_YEAR;
        while (initYear <= actualYear + 1) {
            yearsList.add(valueOf(initYear));
            initYear++;
        }
    }

    public void postProcess(Object document) {
        HSSFWorkbook workbook = (HSSFWorkbook) document;

        formatCells(workbook);
        addSumFormula(workbook.getSheetAt(0));
        addProjectNameColumn(workbook.getSheetAt(0));
        addConfirmColumn(workbook.getSheetAt(0));
        resizeColumns(workbook.getSheetAt(0));
        addNameAndDateForSignature(workbook.getSheetAt(0));
    }

    private void addProjectNameColumn(HSSFSheet sheet) {
        HSSFRow row = sheet.getRow(0);
        HSSFCell projectNameCell = row.createCell(6);
        projectNameCell.setCellValue("Project Name");

        for (int i = 0; i < confirmedRecords.size(); i++) {
            HSSFCell cell = sheet.getRow(i + 1).createCell(6);
            Record record = confirmedRecords.get(i);
            cell.setCellValue(record.getProject().getProjectName());
        }
    }

    private void addConfirmColumn(HSSFSheet sheet) {
        HSSFRow row = sheet.getRow(0);
        HSSFCell projectNameCell = row.createCell(7);
        projectNameCell.setCellValue("Confirmed");

        for (int i = 0; i < confirmedRecords.size(); i++) {
            HSSFCell cell = sheet.getRow(i + 1).createCell(7);
            Record record = confirmedRecords.get(i);
            if (record.isAuthorized())
                cell.setCellValue("Confirmed");
            else
                cell.setCellValue("Unconfirmed");
        }
    }

    private void refreshTable() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = facesContext.getViewRoot();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        UIComponent tableComponent = viewRoot.findComponent(DATA_TABLE_ID);
        requestContext.update(tableComponent.getClientId());
        refreshExportToExcelButton();
    }

    private void refreshExportToExcelButton() {
        RequestContext.getCurrentInstance().update(EXPORT_TO_EXCEL_BUTTON_ID);
    }

    private void loadMessages() {
        Locale locale = loginBean.getLocale();
        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        errorMessage = messages.getString("errorMessage");
        successfulMessage = messages.getString("successfulMessage");
        recordsConfirmedMessage = messages.getString("recordsConfirmedMessage");
        selectRecordMessage = messages.getString("selectRecordMessage");
        notAvailableConfirmationMessage = messages.getString("notAvailableConfirmationMessage");
        finalText = messages.getString("finalText");
        notFinalText = messages.getString("notFinalText");
        confirmedText = messages.getString("confirmedText");
        unConfirmedText = messages.getString("unConfirmedText");
    }

    private void setUpPaginator() {
        paginatorList = new ArrayList<>();
        paginatorList.add("10");
        paginatorList.add("25");
        paginatorList.add("All");
    }

    private void resizeColumns(HSSFSheet sheet) {
        HSSFRow row = sheet.getRow(0);
        for (int colNum = 0; colNum < row.getLastCellNum(); colNum++) {
            HSSFCellStyle style = sheet.getColumnStyle(colNum);
            sheet.autoSizeColumn(colNum);
        }
    }

    private void formatCells(HSSFWorkbook workbook) {
        CreationHelper creationHelper = workbook.getCreationHelper();
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        int nextRow = workbook.getSheetAt(0).getPhysicalNumberOfRows();
        workbook.getSheetAt(0).getRow(0).getCell(4).setCellValue("Pause T.");
        for (int i = 1; i < nextRow; i++) {
            HSSFCell cell = workbook.getSheetAt(0).getRow(i).getCell(5);
            cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("0.0"));
            cell.setCellStyle(cellStyle);
            cell.setCellValue(Double.valueOf(cell.getStringCellValue()));
        }
    }

    private void addSumFormula(HSSFSheet sheet) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        int nextRow = sheet.getPhysicalNumberOfRows();
        sheet.createRow(nextRow);
        HSSFCell cell = sheet.getRow(nextRow).createCell(5);
        cell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
        cell.setCellFormula("SUM(F2:F" + nextRow + ")");
        String formattedFloat = decimalFormat.format(cell.getNumericCellValue());
        formattedFloat = formattedFloat.replace(",", ".");
        Float sum = Float.parseFloat(formattedFloat);
        System.out.println(sum);
        cell.setCellValue(sum);

        HSSFCellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        HSSFFont hSSFFont = sheet.getWorkbook().createFont();
        hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        cellStyle.setFont(hSSFFont);
        cell.setCellStyle(cellStyle);
    }

    private void addNameAndDateForSignature(HSSFSheet sheet) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyy");
        int nextRow = sheet.getPhysicalNumberOfRows() + 4;
        sheet.createRow(nextRow);
        String name = sheet.getRow(1).getCell(0).getStringCellValue();

        Calendar calendar = Calendar.getInstance();
        HSSFCell cellForDate = sheet.getRow(nextRow).createCell(0);
        Date date = calendar.getTime();
        String dateStr = simpleDateFormat.format(date);
        cellForDate.setCellValue(dateStr);
        int rowIndexForName = nextRow + 1;
        sheet.createRow(rowIndexForName);
        HSSFCell cellForName = sheet.getRow(rowIndexForName).createCell(0);
        cellForName.setCellValue(name);

        HSSFCell cellForAdmin = sheet.getRow(rowIndexForName).createCell(6);
        cellForAdmin.setCellValue(loggedUsersName);
        HSSFCell cellForAdminDate = sheet.getRow(nextRow).createCell(6);
        cellForAdminDate.setCellValue(dateStr);

    }

    public List<Record> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<Record> recordsList) {
        this.recordsList = recordsList;
    }

    public List<String> getProjectsNameList() {
        return projectsNameList;
    }

    public void setProjectsNameList(List<String> projectsNameList) {
        this.projectsNameList = projectsNameList;
    }

    public List<String> getMonthsList() {
        return monthsList;
    }

    public void setMonthsList(List<String> monthsList) {
        this.monthsList = monthsList;
    }

    public List<String> getYearsList() {
        return yearsList;
    }

    public void setYearsList(List<String> yearsList) {
        this.yearsList = yearsList;
    }

    public List<String> getTypesOfData() {
        return typesOfData;
    }

    public void setTypesOfData(List<String> typesOfData) {
        this.typesOfData = typesOfData;
    }

    public String getLoggedUsersName() {
        return loggedUsersName;
    }

    public void setLoggedUsersName(String loggedUsersName) {
        this.loggedUsersName = loggedUsersName;
    }

    public List<Record> getConfirmedRecords() {
        return confirmedRecords;
    }

    public void setConfirmedRecords(List<Record> confirmedRecords) {
        this.confirmedRecords = confirmedRecords;
    }

    public List<String> getSelectedProjects() {
        return selectedProjects;
    }

    public void setSelectedProjects(List<String> selectedProjects) {
        this.selectedProjects = selectedProjects;
    }

    public List<String> getSelectedMonths() {
        return selectedMonths;
    }

    public void setSelectedMonths(List<String> selectedMonths) {
        this.selectedMonths = selectedMonths;
    }

    public List<String> getSelectedYears() {
        return selectedYears;
    }

    public void setSelectedYears(List<String> selectedYears) {
        this.selectedYears = selectedYears;
    }

    public List<String> getSelectedDataTypes() {
        return selectedDataTypes;
    }

    public void setSelectedDataTypes(List<String> selectedDataTypes) {
        this.selectedDataTypes = selectedDataTypes;
    }

    public List<String> getSelectedNames() {
        return selectedNames;
    }

    public void setSelectedNames(List<String> selectedNames) {
        this.selectedNames = selectedNames;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<String> getSelectedTypeOfFinalization() {
        return selectedTypeOfFinalization;
    }

    public boolean isRecordsSelected() {
        return recordsSelected;
    }

    public void setRecordsSelected(boolean recordsSelected) {
        this.recordsSelected = recordsSelected;
    }

    public List<User> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(List<User> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public void setSelectedTypeOfFinalization(List<String> selectedTypeOfFinalization) {
        this.selectedTypeOfFinalization = selectedTypeOfFinalization;
    }

    public List<String> getTypesOfFinalization() {
        return typesOfFinalization;
    }

    public void setTypesOfFinalization(List<String> typesOfFinalization) {
        this.typesOfFinalization = typesOfFinalization;
    }

    public List<String> getPaginatorList() {
        return paginatorList;
    }

    public void setPaginatorList(List<String> paginatorList) {
        this.paginatorList = paginatorList;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public boolean isPaginator() {
        return paginator;
    }

    public void setPaginator(boolean paginator) {
        this.paginator = paginator;
    }
}
