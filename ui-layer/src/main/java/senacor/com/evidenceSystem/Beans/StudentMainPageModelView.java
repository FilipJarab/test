package senacor.com.evidencesystem.beans;

import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * This class represents a managed bean for student main page. Student can enter into his project or he can go to the
 * Manage Profile page.
 * The xhtml file for this managed bean is the student_main_page.xhtml.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 13.06.2016.
 */
@Named
@RequestScoped
public class StudentMainPageModelView implements Serializable {
    private String userName;
    private User user;
    private String projectName;
    private String userRank;
    private Project project;
    private String contractEndDate;
    private boolean enterProjectButtonDisabled;

    @Inject
    private EvidenceService evidenceService;

    @Inject
    private LoginBean loginBean;

    @PostConstruct
    public void create() {
        HttpServletRequest request = getServletRequest();
        if (request.isUserInRole("ADMIN")) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("office_manager_main_page.xhtml");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String usersEmail = request.getUserPrincipal().toString();
        user = evidenceService.getUserByEmail(usersEmail);
        userName = user.getFirstName() + " " + user.getLastName();
        loadDataFromDB();
    }

    public String enterActualProject() {
        return "student_project_page?faces-redirect=true";
    }

    public String getContractEndDate() {
        return contractEndDate;
    }

    public HttpServletRequest getServletRequest() {
        FacesContext context = getFacesContext();
        return (HttpServletRequest)
                context.getExternalContext().getRequest();
    }

    public FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getUserRank() {
        userRank = user.getRank().getName();
        return userRank;
    }

    public void setUserRank(String userRank) {
        this.userRank = userRank;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    private void loadDataFromDB() {
        enterProjectButtonDisabled = true;
        project = evidenceService.getProjectByUser(user);
        Profile profile = evidenceService.getProfileByUser(user);
        if (!(project.isNull()) && !(profile.isNull())) {
            projectName = project.getProjectName();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            contractEndDate = dateFormat.format(evidenceService.getProfileByUser(user).getDateOfEnd());
            enterProjectButtonDisabled = false;
        }
    }

    public boolean isEnterProjectButtonDisabled() {
        return enterProjectButtonDisabled;
    }

    public void setEnterProjectButtonDisabled(boolean isEnterProjectButtonEnabled) {
        this.enterProjectButtonDisabled = isEnterProjectButtonEnabled;
    }
}


