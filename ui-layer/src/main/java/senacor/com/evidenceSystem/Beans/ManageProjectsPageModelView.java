package senacor.com.evidencesystem.beans;

import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.exception.ProjectNameAlreadyExistsException;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Page for editing projects and creating new projects. Available only to ADMIN users.
 * Created by fjarab on 06.07.2016.
 */
@Named
@ViewScoped
public class ManageProjectsPageModelView implements Serializable {
    private final String HIDE_DIALOG_COMMAND = "PF('confirm').hide();";
    private final String PROJECTS_TABLE = "projectsForm:projectsTable";
    private String projectName;
    private String anotherNameMessage;
    private String projectAlreadyExistsMessage;
    private String projectCreatedMessage;
    private String successfulMessage;
    private String projectNameAlreadyExistMessage;
    private String errorMessage;
    private String projectUpdatedMessage;
    private String noNameMessage;
    private List projectsList;

    @Inject
    private EvidenceService evidenceService;

    @Inject
    private LoginBean loginBean;

    @PostConstruct
    public void init() {
        loadMessages();
        setProjectsList(evidenceService.getAllProjects());
    }

    private void loadMessages() {
        Locale locale = loginBean.getLocale();
        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        anotherNameMessage = messages.getString("anotherNameMessage");
        projectAlreadyExistsMessage = messages.getString("projectAlreadyExistsMessage");
        projectCreatedMessage = messages.getString("projectCreatedMessage");
        successfulMessage = messages.getString("successfulMessage");
        projectUpdatedMessage = messages.getString("projectUpdatedMessage");
        projectNameAlreadyExistMessage = messages.getString("projectNameAlreadyExistMessage");
        errorMessage = messages.getString("errorMessage");
        noNameMessage = messages.getString("noNameMessage");
    }

    public void createProject() {
        if ("".equals(projectName)) {
            FacesMessage facesMessage = new FacesMessage(errorMessage, noNameMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("growl", facesMessage);
            RequestContext.getCurrentInstance().execute(HIDE_DIALOG_COMMAND);
            return;
        }
        Project newProject = new Project();
        newProject.setProjectName(projectName);
        if (!doExist(newProject)) {
            FacesMessage facesMessage = new FacesMessage(projectCreatedMessage, "Project " + newProject.getProjectName() + " successfully created!");
            facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage("growl", facesMessage);
            RequestContext.getCurrentInstance().execute(HIDE_DIALOG_COMMAND);
            projectsList.add(newProject);
            evidenceService.insertEntity(newProject);
        } else {
            FacesMessage facesMessage = new FacesMessage(projectAlreadyExistsMessage, anotherNameMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("growl", facesMessage);
            RequestContext.getCurrentInstance().execute(HIDE_DIALOG_COMMAND);
        }
        refreshProjectsTable();
    }

    public boolean doExist(Project newProject) {
        List<Project> allProjects = evidenceService.getAllProjects();
        for (Project project : allProjects) {
            if (project.getProjectName().equals(newProject.getProjectName()))
                return true;
        }
        return false;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public List getProjectsList() {
        return projectsList;
    }

    public void setProjectsList(List projectsList) {
        this.projectsList = projectsList;
    }

    public void onRowEdit(RowEditEvent event) {
        Project project = (Project) event.getObject();
        try {
            evidenceService.updateProjectName(project, project.getProjectName());
            FacesMessage facesMessage = new FacesMessage(successfulMessage, projectUpdatedMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage("growl", facesMessage);
        } catch (ProjectNameAlreadyExistsException e) {
            FacesMessage facesMessage = new FacesMessage(errorMessage, projectNameAlreadyExistMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("growl", facesMessage);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            refreshPage();
        }
    }

    private void refreshPage() {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void refreshProjectsTable() {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = context.getViewRoot();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        UIComponent tableComponent = viewRoot.findComponent(PROJECTS_TABLE);
        requestContext.update(tableComponent.getClientId());
    }
}
