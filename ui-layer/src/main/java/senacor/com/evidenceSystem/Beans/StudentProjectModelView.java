package senacor.com.evidencesystem.beans;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DateViewChangeEvent;
import org.primefaces.event.RowEditEvent;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.RecordFromFutureException;
import senacor.com.evidencesystem.exception.RecordFromPastException;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import static java.util.Arrays.asList;

/**
 * Created by fjarab on 02.06.2016.
 * Backing bean for student_project_page.xhtml, allows student to see his records and add new records.
 */

@Named
@ViewScoped
public class StudentProjectModelView implements Serializable {
    private final String TABLE_ID = "tableForm:recordsTable";
    private final String PUT_DATA_BUTTON = "inputsForm:putDataButton";
    private final String LOCK_BUTTON = "tableForm:recordsTable:lockButton";
    private final String DELETE_BUTTON = "tableForm:recordsTable:deleteButton";
    private final String TIME_INPUTS = "inputsForm:timeInputs";
    private final String TOTAL_HOURS_LABEL = "tableForm:recordsTable:totalHours";
    private final String INPUT_FORM = "inputsForm";
    private Project actualProject;
    private User user;
    private RequestContext requestContext;
    private TypeOfWork typeOfWork;
    private List<TypeOfWork> typesList = asList(TypeOfWork.TESTING, TypeOfWork.BUGFIXING,
            TypeOfWork.SUPPORTING, TypeOfWork.SETUP, TypeOfWork.PROGRAMMING, TypeOfWork.LEARNING);
    private Date date = new Date();
    private DateTime startTime = new DateTime();
    private DateTime endTime = new DateTime();
    private DateTime pause = new DateTime();
    private List<Record> records = new ArrayList();
    private String info;
    private float totalHours;
    private String successfulMessage;
    private String recordDeletedMessage;
    private String recordsStoredMessage;
    private String errorMessage;
    private String futureLockMessage;
    private String pastLockMessage;
    private List<String> projectsNames;
    private boolean recordIsLocked;

    @Inject
    private EvidenceService evidenceService;

    @Inject
    private LoginBean loginBean;

    @PostConstruct
    public void init() {
        loadMessages();
        loadData();
        clearDates();
        updateUI();
    }

    private void loadData() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)
                context.getExternalContext().getRequest();
        user = evidenceService.getUserByEmail(request.getRemoteUser());
        info = user.getFirstName() + " " + user.getLastName() + " (" + user.getRank() + ")";
        projectsNames = new ArrayList<>();
        for (Project project : evidenceService.getAllProjects()) {
            projectsNames.add(project.getProjectName());
        }
        reloadRecordsFromDB();
        totalHours = evidenceService.calculateTotalHours(user, date);
        setActualProject(evidenceService.getProjectByUser(user));
        setRecordLocked();
    }

    private void loadMessages() {
        Locale locale = loginBean.getLocale();
        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);
        successfulMessage = messages.getString("successfulMessage");
        recordDeletedMessage = messages.getString("recordDeletedMessage");
        recordsStoredMessage = messages.getString("recordsStoredMessage");
        errorMessage = messages.getString("errorMessage");
        futureLockMessage = messages.getString("futureLockMessage");
        pastLockMessage = messages.getString("pastLockMessage");
    }

    private void updateUI() {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = context.getViewRoot();
        requestContext = RequestContext.getCurrentInstance();

        UIComponent putDataButton = viewRoot.findComponent(PUT_DATA_BUTTON);
        UIComponent hours = viewRoot.findComponent(TOTAL_HOURS_LABEL);
        System.out.println(hours);
        UIComponent tableComponent = viewRoot.findComponent(TABLE_ID);
        UIComponent lockButton = viewRoot.findComponent(LOCK_BUTTON);
        UIComponent deleteButton = viewRoot.findComponent(DELETE_BUTTON);
        UIComponent inputForm = viewRoot.findComponent(INPUT_FORM);

        if (!records.isEmpty() && records.get(0).isLocked()) {
            deleteButton.setRendered(false);
            lockButton.setRendered(false);
        } else {
            putDataButton.setRendered(true);
            deleteButton.setRendered(true);
            lockButton.setRendered(true);
        }
        if (!records.isEmpty() && records.get(0).isLocked())
            recordIsLocked = true;
        else recordIsLocked = false;
        requestContext.update(tableComponent.getClientId());
        requestContext.update(putDataButton.getClientId());
        requestContext.update(inputForm.getClientId());
        requestContext.update(hours.getClientId());
    }

    public void putData() {
        Record newRecord = new Record();
        newRecord.setUser(user);
        List<Project> allProjects = evidenceService.getAllProjects();
        Project projectToSet = null;
        for (Project project : allProjects) {
            if (project.getProjectName().equals(actualProject.getProjectName()))
                projectToSet = project;
        }
        newRecord.setProject(projectToSet);
        newRecord.setDate(date);
        newRecord.setTimeOfStart(getStartTime());
        newRecord.setTimeOfEnd(getEndTime());
        newRecord.setDurationOfPause(getPause());
        newRecord.setTypeOfWork(typeOfWork.toString());
        newRecord.setAuthorized(false);
        newRecord.setLocked(false);

        evidenceService.insertEntity(newRecord);
        records.add(newRecord);
        preSetTimesForNewRecord();

        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = context.getViewRoot();

        requestContext = RequestContext.getCurrentInstance();
        UIComponent tableComponent = viewRoot.findComponent(TABLE_ID);
        requestContext.update(tableComponent.getClientId());

        UIComponent timeInputComponent = viewRoot.findComponent(TIME_INPUTS);
        requestContext.update(timeInputComponent.getClientId());

        totalHours = evidenceService.calculateTotalHours(user, date);
        UIComponent totalHoursComponent = viewRoot.findComponent(TOTAL_HOURS_LABEL);
        requestContext.update(totalHoursComponent.getClientId());

        context.addMessage(null, new FacesMessage(successfulMessage, recordsStoredMessage));
    }

    private void clearDates() {
        startTime = new DateTime(DateTimeZone.forID("Europe/Bratislava")).withTimeAtStartOfDay().withHourOfDay(8);
        endTime = new DateTime(DateTimeZone.forID("Europe/Bratislava")).withTimeAtStartOfDay().withHourOfDay(17);
        pause = new DateTime(DateTimeZone.forID("Europe/Bratislava")).withTimeAtStartOfDay().withMinuteOfHour(30);
    }


    private void preSetTimesForNewRecord() {
        Record lastRecord = records.get(records.size() - 1);
        setStartTime(lastRecord.getTimeOfStart());
        setEndTime(lastRecord.getTimeOfEnd());
        setPause(lastRecord.getDurationOfPause());
    }

    public void lockMonth() {
        try {
            evidenceService.lockRecords(records);
        } catch (RecordFromFutureException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, futureLockMessage));
        } catch (RecordFromPastException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, pastLockMessage));
        }
        records = evidenceService.getRecordsByCurrentMonth(user, date);
        updateUI();
    }

    public boolean isRecordIsLocked() {
        return recordIsLocked;
    }

    public void setRecordLocked() {
        if (!records.isEmpty() && records.get(0).isLocked())
            recordIsLocked = true;
        else recordIsLocked = false;
    }

    public void setRecordIsLocked(boolean recordIsLocked) {
        this.recordIsLocked = recordIsLocked;
    }

    public TypeOfWork getTypeOfWork() {
        return typeOfWork;
    }

    public void setTypeOfWork(TypeOfWork typeOfWork) {
        this.typeOfWork = typeOfWork;
    }

    public List<TypeOfWork> getTypesList() {
        return typesList;
    }

    public void setTypesList(List<TypeOfWork> typesList) {
        this.typesList = typesList;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        this.date = date;
    }

    public void monthChanged(DateViewChangeEvent event) {
        DateTime newMonth = new DateTime().withMonthOfYear(event.getMonth()).withDayOfMonth(13).withDayOfWeek(3).withYear(event.getYear());
        date.setTime(newMonth.toDate().getTime());
        records = evidenceService.getRecordsByCurrentMonth(user, newMonth.toDate());
        totalHours = evidenceService.calculateTotalHours(user, newMonth.toDate());

        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = context.getViewRoot();

        requestContext = RequestContext.getCurrentInstance();
        UIComponent tableComponent = viewRoot.findComponent(TABLE_ID);
        requestContext.update(tableComponent.getClientId());
        updateUI();
    }


    public void onRowEdit(RowEditEvent event) {
        Record editedRecord = (Record) event.getObject();
        evidenceService.updateRecordsPauseDuration(editedRecord, editedRecord.getDurationOfPause());
        evidenceService.updateRecordsTimeOfStart(editedRecord, editedRecord.getTimeOfStart());
        evidenceService.updateRecordsTimeOfEnd(editedRecord, editedRecord.getTimeOfEnd());
        evidenceService.updateRecordsTypeOfWork(editedRecord, editedRecord.getTypeOfWork());
        reloadRecordsFromDB();
        setTotalHours(evidenceService.calculateTotalHours(user, date));
        updateUI();
        reloadPage();
    }

    public Date getStartTime() {
        return startTime.toDate();
    }

    public void setStartTime(Date startTime) {
        this.startTime = new DateTime(startTime);
    }

    public Date getEndTime() {
        return endTime.toDate();
    }

    public void setEndTime(Date endTime) {
        this.endTime = new DateTime(endTime);
    }

    private void reloadRecordsFromDB() {
        records.clear();
        records.addAll(evidenceService.getRecordsByCurrentMonth(user, new Date()));
    }

    public Date getPause() {
        return pause.toDate();
    }

    public void setPause(Date pause) {
        this.pause = new DateTime(pause);
    }

    public List getRecords() {
        return records;
    }

    public void setRecords(List records) {
        this.records = records;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public float getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(float totalHours) {
        this.totalHours = totalHours;
    }

    public void deleteRecord(Record recordToDelete) {
        evidenceService.deleteEntity(recordToDelete);
        records = evidenceService.getRecordsByCurrentMonth(user, date);

        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = context.getViewRoot();
        requestContext = RequestContext.getCurrentInstance();
        UIComponent tableComponent = viewRoot.findComponent(TABLE_ID);
        requestContext.update(tableComponent.getClientId());
        totalHours = evidenceService.calculateTotalHours(user, date);

        context.addMessage(null, new FacesMessage(successfulMessage, recordDeletedMessage));
    }

    private void reloadPage() {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getProjectsNames() {
        return projectsNames;
    }

    public void setProjectsNames(List<String> projects) {
        this.projectsNames = projects;
    }

    public Project getActualProject() {
        return actualProject;
    }

    public void setActualProject(Project actualProject) {
        this.actualProject = actualProject;
    }
}
