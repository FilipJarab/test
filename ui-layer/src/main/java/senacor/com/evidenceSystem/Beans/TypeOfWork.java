package senacor.com.evidencesystem.beans;

/**
 * Created by fjarab on 27.06.2016.
 * Enum types of work
 */

public enum TypeOfWork {
    TESTING, PROGRAMMING, SUPPORTING, SETUP, BUGFIXING, LEARNING
}
