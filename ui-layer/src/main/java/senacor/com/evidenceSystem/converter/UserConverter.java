package senacor.com.evidencesystem.converter;

import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.services.EvidenceService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by dbozo on 01.08.2016.
 * Custom faces converter for user selection( Office manager main page).
 * Converter is for: main_data_table_form:users_name_menu element.
 * Returning required User object from DB.
 */
@Named(value = "senacor.com.evidencesystem.converter.UserConverter")
public class UserConverter implements Converter {

    @Inject
    private EvidenceService evidenceService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value != null && !value.equals(""))
            return evidenceService.getUserByEmail(value);
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((User) value).getEmail();
    }
}
