package senacor.com.evidencesystem.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents the JSF faces validator for email address validation.
 * Created by dbozo on 31.05.2016.
 */
@FacesValidator("emailAddressValidate")
public class EmailAddressValidate implements Validator {
    private String invalidEmailMessage;

    public EmailAddressValidate() {
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        invalidEmailMessage = messages.getString("invalidEmailMessage");
    }

    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        validateEmailAddress(value);
    }

    public void validateEmailAddress(Object value) {
        String emailAddress = value.toString();
        Pattern pattern = Pattern.compile("[a-zA-Z]+[.-][a-zA-z]+@senacor.com");
        Matcher matcher = pattern.matcher(emailAddress);
        if (!matcher.matches() && emailAddress != null) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, invalidEmailMessage, null));
        }
    }
}
