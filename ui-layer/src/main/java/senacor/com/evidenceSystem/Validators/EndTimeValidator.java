package senacor.com.evidencesystem.validators;

/**
 * Created by dbozo on 09.11.2016.
 */

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

@FacesValidator("endTimeValidator")
public class EndTimeValidator implements Validator {
    private String invTimeRangeMessage;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        invTimeRangeMessage = messages.getString("timeRangeMessage");
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
        if (value == null) {
            return;
        }
        Object startDateValue = component.getAttributes().get("startTime");
        if (startDateValue == null) {
            return;
        }
        Date startDate = (Date) startDateValue;
        Date endDate = (Date) value;
        String endTime = localDateFormat.format(endDate);
        String startTime = localDateFormat.format(startDate);
        try {
            startDate = localDateFormat.parse(startTime);
            endDate = localDateFormat.parse(endTime);
        } catch (ParseException e) {
            System.out.println("invalid dates");
        }

        if (endDate.before(startDate)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, invTimeRangeMessage, null));
        }
    }
}