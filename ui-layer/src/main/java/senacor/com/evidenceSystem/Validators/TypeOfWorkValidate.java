package senacor.com.evidencesystem.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import java.util.ResourceBundle;

/**
 * JSF Validator for TypeOfWork selection on student_project_page
 * Created by fjarab on 15.07.2016.
 */
@FacesValidator("typeOfWorkValidate")
public class TypeOfWorkValidate implements Validator {
    ResourceBundle messages = ResourceBundle.getBundle("messages");

    public TypeOfWorkValidate() {

    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null) {
            FacesMessage message = new FacesMessage("Error!",
                    messages.getString("selectTypeMessage"));
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }

    }
}
