package senacor.com.evidencesystem.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by dbozo on 14.11.2016.
 */

@FacesValidator("startTimeValidator")
public class StartTimeValidator implements Validator {
    private String invTimeRangeMessage;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
        ResourceBundle messages = ResourceBundle.getBundle("messages");
        invTimeRangeMessage = messages.getString("timeRangeMessage");
        if (value == null) {
            return;
        }
        Object endTime = component.getAttributes().get("endTime");
        if (endTime == null) {
            return;
        }
        Date endDate = (Date) endTime;
        Date startDate = (Date) value;
        String endTimeStr = localDateFormat.format(endDate);
        String startTime = localDateFormat.format(startDate);
        try {
            startDate = localDateFormat.parse(startTime);
            endDate = localDateFormat.parse(endTimeStr);
        } catch (ParseException e) {
            System.out.println("invalid dates");
        }

        if (endDate.before(startDate)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, invTimeRangeMessage, null));
        }
    }
}
