package senacor.com.evidencesystem.validators;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.faces.validator.ValidatorException;

/**
 * Unit test class for email address validation.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 13.06.2016.
 */
public class EmailAddressValidateTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    EmailAddressValidate validator = new EmailAddressValidate();

    @Test
    public void testValidate() throws Exception {
        System.out.println("validate");
        validator.validate(null, null, "Dn.Nc@senacor.com");

     //   exception.expect(ValidatorException.class);
        validator.validate(null, null, "dineud-udnue@senacor.com");
    }

    @Test
    public void testValidateEmailAddress() throws Exception {
        System.out.println("validateEmailAddress");
        validator.validateEmailAddress("nnnm-Vdededec@senacor.com");


    }
}

