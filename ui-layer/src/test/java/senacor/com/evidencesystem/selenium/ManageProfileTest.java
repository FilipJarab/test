package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Selenium test class for Manage Profile Page.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 21.06.2016.
 */
public class ManageProfileTest {
    @Before
    public void get() {
        initDB();
        goToPage();
        login("Jozef.Hrozny@senacor.com", "heslo");
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_button')]")).click();
    }

    @Test
    public void updateDataTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'customRadio')]/tbody/tr/td[1]/div/div[2]/span")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_calendar_start_date_input')]")).sendKeys("12.1.2015");
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_calendar_end_date_input')]")).sendKeys("11.2.2017");
        webDriver.findElement(By.xpath(".//*[contains(@id,'apply_settings_button')]")).click();
    }

    @Test
    public void goToHomePageTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'home_page_link')]")).click();
        String text = webDriver.findElement(By.xpath(".//*[contains(@id,'welcome_text_student')]")).getText();
        String welcomeString = "Welcome Jozef Hrozny! (Student)";
        assertEquals(welcomeString, text);
    }

    @Test
    public void logoutTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'logout_button_manage_profile')]")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = webDriver.getTitle();
        assertEquals("Senacor Evidence", title);
    }

    @After
    public void quit() {
        webDriver.quit();
    }
}
