package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Selenium test for the all application.
 * Created by dbozo on 21.07.2016.
 *
 * @author Daniel Bozo
 */

public class ManiApplicationTest {

    @Before
    public void get() {
        initDB();
        goToPage();
    }

    @Test
    public void mainApplicationTest() {

        /* Registration */
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Testing");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User student = new User("Jaro.Velky@senacor.com", "heslo");
        User admin = new User("Anna.Simova@senacor.com", "heslo");
        Project project = new Project();
        Project project1 = new Project();
        project.setProjectName("The Project");
        project1.setProjectName("Senacor Project");
        entityManager.getTransaction().begin();
        entityManager.persist(student);
        entityManager.persist(admin);
        entityManager.persist(project);
        entityManager.persist(project1);
        entityManager.getTransaction().commit();

        /* Login new user */
        login("Jaro.Velky@senacor.com", "heslo");
        String correctMessageLogin = "Welcome Jaro Velky! (Student)";
        String messageLogin = webDriver.findElement(By.xpath(".//*[contains(@id,'welcome_text_student')]")).getText();
        assertEquals(correctMessageLogin, messageLogin);

         /* Manage profile */
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_button')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'j_idt11:customRadio')]/tbody/tr/td[1]/div/div[2]/span")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'j_idt11:manage_profile_calendar_start_date_input')]")).click();
        webDriver.switchTo().activeElement();
        webDriver.findElement(By.xpath(".//*[contains(@id,'ui-datepicker-div)']/table/tbody/tr[4]/td[1]/a")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'j_idt11:manage_profile_calendar_end_date_input')]")).click();
        webDriver.switchTo().activeElement();
        webDriver.findElement(By.xpath(".//*[contains(@id,'ui-datepicker-div)']/table/tbody/tr[4]/td[2]/a")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'apply_settings_button')]")).click();

         /* Home Page */
        webDriver.findElement(By.xpath(".//*[contains(@id,'home_page_link')]")).click();
        String contractEndsInfoCorrect = "Your contract ends on: 12.07.2017";
        String contractEndsInfo = webDriver.findElement(By.xpath(".//*[contains(@id,'contract_end_date_student_m_page')]")).getText();
        assertEquals(contractEndsInfoCorrect, contractEndsInfo);

         /* Enter Project */
        webDriver.findElement(By.xpath(".//*[contains(@id,'enter_project_button')]")).click();
        String correctWelcomeText = "Welcome Jaro STUDENT";
        String welcomeText = webDriver.findElement(By.xpath(".//*[contains(@id,'welcome_text_student_project_page')]")).getText();
        assertEquals(correctWelcomeText, welcomeText);

        /* Manage Profile from Project page */
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_button_project_page')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_calendar_end_date_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_calendar_end_date_input')]")).sendKeys("20.07.17");
        webDriver.findElement(By.xpath(".//*[contains(@id,'apply_settings_button')]")).click();
        String successfulUpdateProjectPage = webDriver.findElement(By.xpath(".//*[contains(@id,'successful_update_message')]/div/ul/li/span[contains(.,'successfully')]")).getText();
        String correctMessageSuccessUpdateProjectPage = "Data successfully updated!";
        assertEquals(correctMessageSuccessUpdateProjectPage, successfulUpdateProjectPage);

        webDriver.findElement(By.xpath(".//*[contains(@id,'home_page_link')]")).click();
        String contractEndsInfoCorrectProjectP = "Your contract ends on: 20.07.2017";
        String contractEndsInfoProjectP = webDriver.findElement(By.xpath(".//*[contains(@id,'contract_end_date_student_m_page')]")).getText();
        assertEquals(contractEndsInfoCorrectProjectP, contractEndsInfoProjectP);

        /* Enter project after profile update. Put a record */
        webDriver.findElement(By.xpath(".//*[contains(@id,'enter_project_button')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:inline_inline')]/div/table/tbody/tr[1]/td[6]/a")).click();
        String date = webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:timeInputs')]/tbody/tr[1]/td[2]")).getText();
        assertEquals("01/07/2016", date);

        webDriver.findElement(By.xpath(".//*[contains(@id,'startTime_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'startTime_input')]")).sendKeys("09:00");
        webDriver.findElement(By.xpath(".//*[contains(@id,'endTime_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'endTime_input')]")).sendKeys("18:00");
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:pauseTime_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:pauseTime_input')]")).sendKeys("00:30");
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:inline_inline')]/div/table/tbody/tr[1]/td[6]/a")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'typeOfWorkInput')]/div[3]/span")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'typeOfWorkInput_4')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'putDataButton')]")).click();

        String dateP = webDriver.findElement(By.xpath(".//*[contains(@id,'0:record_date_DT')]")).getText();
        String projectName = webDriver.findElement(By.xpath(".//*[contains(@id,'0:project_name_DT')]")).getText();
        String startTime = webDriver.findElement(By.xpath(".//*[contains(@id,'0:hour_from_DT')]")).getText();
        String endTime = webDriver.findElement(By.xpath(".//*[contains(@id,'0:hour_to_DT')]")).getText();
        String typeOfWork = webDriver.findElement(By.xpath(".//*[contains(@id,'0:type_of_work_DT')]")).getText();
        String pauseTime = webDriver.findElement(By.xpath(".//*[contains(@id,'0:duration_of_pause_DT')]")).getText();
        assertEquals("09:00", startTime);
        assertEquals("18:00", endTime);
        assertEquals("SETUP", typeOfWork);
        assertEquals("00:30", pauseTime);
        assertEquals("01/07/2016", dateP);
        assertEquals(projectName, "The Project");

        /* Login as Manager */
        webDriver.findElement(By.xpath(".//*[contains(@id,'logout_b_project_page')]")).click();
        login("Anna.Simova@senacor.com", "heslo");
        String text = webDriver.findElement(By.xpath(".//*[contains(@id,'welcome_text_admin')]")).getText();
        assertEquals("Welcome Anna Simova! (Office Manager)", text);

        /* Filter data */
        webDriver.findElement(By.xpath(".//*[contains(@id,'users_name_menu')]/div[3]/ul/li[3]/div/div/span")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'projects_menu')]/div[3]/ul/li[1]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'moths_menu')]/div[3]/ul/li[7]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'year_menu')]/div[3]/ul/li[7]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'type_of_data_menu')]/div[2]/ul/li[2]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'finalised_menu')]/div[2]/ul/li[2]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'show_resulst_b')]")).click();

        /* Check filtered data */
        String name = webDriver.findElement(By.xpath(".//*[contains(@id,'user_name_data_table')]")).getText();
        String dateM = webDriver.findElement(By.xpath(".//*[contains(@id,'date_data_table')]")).getText();
        String timeOfStart = webDriver.findElement(By.xpath(".//*[contains(@id,'time_of_start_data_table')]")).getText();
        String timeOfEnd = webDriver.findElement(By.xpath(".//*[contains(@id,'time_of_end_data_table')]")).getText();
        String durationOfPause = webDriver.findElement(By.xpath(".//*[contains(@id,'duration_of_pause_data_table')]")).getText();
        String totalTime = webDriver.findElement(By.xpath(".//*[contains(@id,'total_time_data_table')]")).getText();

        assertEquals("Jaro Velky", name);
        assertEquals("2016-07-01", dateM);
        assertEquals("09:00:00", timeOfStart);

        assertEquals("18:00:00", timeOfEnd);
        assertEquals("00:30:00", durationOfPause);
        assertEquals("8.5", totalTime);

        /* Go to manage project page */
        webDriver.findElement(By.xpath(".//*[contains(@id,'create_project_button')]")).click();
        String title = webDriver.getTitle();
        assertEquals("Manage Project", title);

        /* Create new project and rename it */
        webDriver.findElement(By.xpath(".//*[contains(@id,'new_project_name_manage_project')]")).sendKeys("Test Project");
        webDriver.findElement(By.xpath(".//*[contains(@id,'create_project_button')]")).click();
        webDriver.switchTo().activeElement();
        webDriver.findElement(By.xpath(".//*[contains(@id,'yess_button')]")).click();
        webDriver.switchTo().activeElement();
        webDriver.findElement(By.xpath(".//*[contains(@id,'2:edit_p_name_output')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'2:edit_p_name_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'2:edit_p_name_input')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'2:edit_p_name_input')]")).sendKeys("Renamed Project");
        webDriver.findElement(By.xpath(".//*[contains(@id,'2:edit_p_name_input')]")).sendKeys(Keys.ENTER);
        webDriver.switchTo().activeElement();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String renamedProjectName = webDriver.findElement(By.xpath(".//*[contains(@id,'2:edit_p_name_output')]")).getText();
        assertEquals("Renamed Project", renamedProjectName);

        /* Logout */
        webDriver.findElement(By.xpath(".//*[contains(@id,'logout_b_manage_project')]")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title1 = webDriver.getTitle();
        assertEquals("Senacor Evidence", title1);
    }

    @After
    public void quit() {
        webDriver.quit();

    }
}
