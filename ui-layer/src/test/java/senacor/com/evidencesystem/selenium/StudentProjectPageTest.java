package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Created by fjarab on 27.06.2016.
 */
public class StudentProjectPageTest {

    @Before
    public void get() {
        initDB();
        goToPage();
        login("Jozef.Hrozny@senacor.com", "heslo");
        setProject();
        webDriver.findElement(By.xpath(".//*[contains(@id,'enter_project_button')]")).click();
    }

    @Test
    public void putData() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'startTime_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'startTime_input')]")).sendKeys("09:00");
        webDriver.findElement(By.xpath(".//*[contains(@id,'endTime_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'endTime_input')]")).sendKeys("18:00");
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:pauseTime_input')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:pauseTime_input')]")).sendKeys("00:25");
        webDriver.findElement(By.xpath(".//*[contains(@id,'inputsForm:inline_inline')]/div/table/tbody/tr[1]/td[6]/a")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'typeOfWorkInput')]/div[3]/span")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'typeOfWorkInput_4')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'putDataButton')]")).click();

        String date = webDriver.findElement(By.xpath(".//*[contains(@id,'0:record_date_DT')]")).getText();
        String projectName = webDriver.findElement(By.xpath(".//*[contains(@id,'0:project_name_DT')]")).getText();
        String startTime = webDriver.findElement(By.xpath(".//*[contains(@id,'0:hour_from_DT')]")).getText();
        String endTime = webDriver.findElement(By.xpath(".//*[contains(@id,'0:hour_to_DT')]")).getText();
        String typeOfWork = webDriver.findElement(By.xpath(".//*[contains(@id,'0:type_of_work_DT')]")).getText();
        String pauseTime = webDriver.findElement(By.xpath(".//*[contains(@id,'0:duration_of_pause_DT')]")).getText();
        assertEquals("09:00", startTime);
        assertEquals("18:00", endTime);
        assertEquals("SETUP", typeOfWork);
        assertEquals("00:25", pauseTime);
        assertEquals("01/07/2016", date);
        assertEquals(projectName, "The Project");
    }

    @After
    public void quit() {
        webDriver.quit();
    }

    private void setProject() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_button')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'customRadio')]/tbody/tr/td[1]/div/div[2]/span")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_calendar_start_date_input')]")).sendKeys("12.1.2015");
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_calendar_end_date_input')]")).sendKeys("11.2.2017");
        webDriver.findElement(By.xpath(".//*[contains(@id,'apply_settings_button')]")).click();
        webDriver.findElement(By.xpath(".//*[contains(@id,'home_page_link')]")).click();
    }
}
