package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.goToPage;
import static senacor.com.evidencesystem.selenium.PageUtils.webDriver;

/**
 * Selenium test class for Register page.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 24.06.2016.
 */
public class RegisterPageTest {
    @Before
    public void get() {
        goToPage();
        webDriver.findElement(By.xpath(".//*[contains(@id,'sing_up_button')]")).click();
    }

    @Test
    public void wrongMailAddressTest() {
        webDriver.findElement(By.xpath("//*[contains(@id,'emailAddress')]")).sendKeys("ass.e@senacor.com");
        webDriver.findElement(By.xpath(".//*[contains(@id,'register_password_field')]")).sendKeys("ss");
        webDriver.findElement(By.xpath(".//*[contains(@id,'register_repeat_password_field')]")).sendKeys("ss");
        webDriver.findElement(By.xpath(".//*[contains(@id,'singUpButtonRegisterPage')]")).click();
        String message = webDriver.findElement(By.xpath(".//*[contains(@id,'invalid_email_address')]/div/ul/li/span")).getText();
        assertEquals("Invalid e-mail address! Please use your Senacor e-mail address.", message);
    }

    @Test
    public void differentPasswordsTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'emailAddress')]")).sendKeys("Ass.Ac@senacor.com");
        webDriver.findElement(By.xpath(".//*[contains(@id,'register_password_field')]")).sendKeys("d");
        webDriver.findElement(By.xpath(".//*[contains(@id,'register_repeat_password_field')]")).sendKeys("ss");
        webDriver.findElement(By.xpath(".//*[contains(@id,'singUpButtonRegisterPage')]")).click();
        String message = webDriver.findElement(By.xpath(".//*[contains(@id,'different_passwords')]/div/ul/li/span")).getText();
        assertEquals("Entered passwords are different.", message);
    }

    @After
    public void quit() {
        webDriver.quit();
    }
}
