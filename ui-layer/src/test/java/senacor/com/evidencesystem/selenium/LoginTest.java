package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Selenium test class for Login page.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 21.06.2016.
 */
public class LoginTest {
    @Before
    public void get() {
        initDB();
        goToPage();

    }

    @Test
    public void wrongLogin() {
        login("Da.Jk@senacor.com", "hesleeo");
        String text = webDriver.findElement(By.xpath(".//*[contains(@id,'wrong_login')]")).getText();
        assertEquals("Wrong username or password!", text);
    }

    @Test
    public void correctAdminLogin() {
        login("Anna.Simova@senacor.com", "heslo");
        String text = webDriver.findElement(By.xpath(".//*[contains(@id,'welcome_text_admin')]")).getText();
        assertEquals("Welcome Anna Simova! (Office Manager)", text);
    }

    @Test
    public void correctStudentLogin() {
        login("Jozef.Hrozny@senacor.com", "heslo");
        String text = webDriver.findElement(By.xpath(".//*[contains(@id,'welcome_text_student')]")).getText();
        assertEquals("Welcome Jozef Hrozny! (Student)", text);
    }

    @After
    public void quit() {
        webDriver.quit();
    }
}
