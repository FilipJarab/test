package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Selenium test class for Student Main page.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 21.06.2016.
 */
public class StudentMainPageTest {

    @Before
    public void get() {
        initDB();
        goToPage();
        login("Jozef.Hrozny@senacor.com", "heslo");
    }

    @Test
    public void projectNameTest() {
        String projectName = webDriver.findElement(By.xpath(".//*[contains(@id,'actual_project_inf_student_m_page')]")).getText();
        assertTrue(projectName.contains("Your Actual Project Is:"));
    }

    @Test
    public void contractsEndDateTest() {
        String contractEndDate = webDriver.findElement(By.xpath(".//*[contains(@id,'contract_end_date_student_m_page')]")).getText();
        assertTrue(contractEndDate.contains("Your contract ends on:"));
    }

    @Test
    public void goToManagePageTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'manage_profile_button')]")).click();
        String pageTitle = webDriver.getTitle();
        assertEquals("Manage Profile", pageTitle);
    }

    @Test
    public void logOutTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'log_out_b_student_main_p')]")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = webDriver.getTitle();
        assertEquals("Senacor Evidence", title);
    }

    @After
    public void quit() {
        webDriver.quit();
    }
}
