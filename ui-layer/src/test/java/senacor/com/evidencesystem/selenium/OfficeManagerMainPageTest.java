package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Selenium test class for Office Manager main Page - OfficeManagerMainPage bean.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 12.07.2016.
 */
public class OfficeManagerMainPageTest {

    @Before
    public void get() {
        initDB();
        goToPage();
        login("Anna.Simova@senacor.com", "heslo");
    }

    @Test
    public void goToManageProjectPageTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'create_project_button')]")).click();
        String title = webDriver.getTitle();
        assertEquals("Manage Projects", title);
    }

    @Test
    public void logOutTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'logout_b_office_manager')]")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = webDriver.getTitle();
        assertEquals("Senacor Evidence", title);
    }

    @After
    public void quit() {
        webDriver.quit();
    }
}
