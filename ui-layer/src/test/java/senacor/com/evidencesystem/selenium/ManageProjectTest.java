package senacor.com.evidencesystem.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static senacor.com.evidencesystem.selenium.PageUtils.*;

/**
 * Selenium test for Manage project page.
 * Created by dbozo on 21.07.2016.
 *
 * @author Daniel Bozo
 */
public class ManageProjectTest {
    @Before
    public void get() {
        initDB();
        goToPage();
        login("Anna.Simova@senacor.com", "heslo");
        webDriver.findElement(By.xpath(".//*[contains(@id,'create_project_button')]")).click();
    }

    @Test
    public void addNewProjectProjectTest() {
        webDriver.findElement(By.xpath(".//*[contains(@id,'new_project_name_manage_project')]")).sendKeys("Test Project");
        webDriver.findElement(By.xpath(".//*[contains(@id,'create_project_button')]")).click();
        webDriver.switchTo().activeElement();
        webDriver.findElement(By.xpath(".//*[contains(@id,'yess_button')]")).click();
        webDriver.switchTo().activeElement();
        assertEquals("Test Project", webDriver.findElement(By.xpath(".//*[contains(@id,'projectsForm:projectsTable:2:edit_p_name_output')]")).getText());
    }

    @After
    public void quit() {
        webDriver.quit();
    }
}
