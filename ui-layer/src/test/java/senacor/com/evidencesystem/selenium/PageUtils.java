package senacor.com.evidencesystem.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.concurrent.TimeUnit;

/**
 * This class has static methods for load the root http address with web driver and for executing
 * the login into evidence system for every implemented selenium test.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 21.06.2016.
 */
public class PageUtils {
    public static WebDriver webDriver;

    public static void goToPage() {
        System.setProperty("webdriver.chrome.driver", "C://projects//tools//chromedriver.exe");
        System.setProperty("webdriver.phantomjs.driver", "C://Projects//tools//phantomjs-2.1.1-windows//bin//phantomjs.exe");
        Capabilities caps = new DesiredCapabilities();
        ((DesiredCapabilities) caps).setJavascriptEnabled(true);
        ((DesiredCapabilities) caps).setCapability("takesScreenshot", true);
        ((DesiredCapabilities) caps).setCapability(
                PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                "C://Projects//tools//phantomjs-2.1.1-windows//bin//phantomjs.exe"
        );
        webDriver = new PhantomJSDriver(caps);
        webDriver.get("http://localhost:8080/ui-layer-1.0/");
        webDriver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
    }

    public static void login(String login, String pass) {
        webDriver.findElement(By.xpath(".//*[contains(@id,'j_username')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'passwordField')]")).clear();
        webDriver.findElement(By.xpath(".//*[contains(@id,'j_username')]")).sendKeys(login);
        webDriver.findElement(By.xpath(".//*[contains(@id,'passwordField')]")).sendKeys(pass);
        webDriver.findElement(By.xpath(".//*[contains(@id,'login_button')]")).click();
    }

    public static void initDB() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Testing");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User admin = new User("Anna.Simova@senacor.com", "heslo");
        User student = new User("Jozef.Hrozny@senacor.com", "heslo");
        Project project = new Project();
        Project project1 = new Project();
        project.setProjectName("The Project");
        project1.setProjectName("Senacor Project");
        entityManager.getTransaction().begin();
        entityManager.persist(project);
        entityManager.persist(project1);
        entityManager.persist(admin);
        entityManager.persist(student);
        entityManager.getTransaction().commit();
    }
}
