package senacor.com.evidencesystem.beans;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Unit test for class OfficeManagerMainPageModelView for generating years lists and for getting months names.
 *
 * @author Daniel Bozo
 *         Created by dbozo on 27.06.2016.
 */
public class OfficeManagerMainPageModelViewTest {

    @Test
    public void generateYearsListTest() {
        List<String> yearsList = new ArrayList<String>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY");
        String stringDate = simpleDateFormat.format(new Date());
        int i1 = Integer.parseInt(stringDate);
        int rok = 2010;
        while (rok <= i1 + 1) {
            yearsList.add(String.valueOf(rok));
            rok++;
        }
        String[] array = {"2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"};
        assertArrayEquals(yearsList.toArray(), array);
    }

    @Test
    public void getMonthNameTest() {
        String correctStringDate = "05-August-2016";
        Date date = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            date = simpleDateFormat.parse(correctStringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long dateInMilliseconds = date.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
        String stringDate = simpleDateFormat.format(date);
        simpleDateFormat.format(new Date(dateInMilliseconds));
        assertEquals("August", stringDate);
    }


}