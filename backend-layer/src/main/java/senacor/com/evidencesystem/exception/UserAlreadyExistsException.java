package senacor.com.evidencesystem.exception;

/**
 * User already exists exception
 * Created by jbonk on 08.06.2016.
 */
public class UserAlreadyExistsException extends Exception {
    public UserAlreadyExistsException(String message) {
        super(message);
    }
}
