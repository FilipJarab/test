package senacor.com.evidencesystem.exception;

/**
 * Record from future exception
 * Created by jbonk on 27-Jul-16.
 */
public class RecordFromFutureException extends Exception {
    public RecordFromFutureException(String message) {
        super(message);
    }
}