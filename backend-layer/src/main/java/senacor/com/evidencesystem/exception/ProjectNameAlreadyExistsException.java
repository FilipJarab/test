package senacor.com.evidencesystem.exception;

/**
 * Project name already exists exception
 * Created by jbonk on 09-Aug-16.
 */
public class ProjectNameAlreadyExistsException extends Exception {
    public ProjectNameAlreadyExistsException(String message) {
        super(message);
    }
}
