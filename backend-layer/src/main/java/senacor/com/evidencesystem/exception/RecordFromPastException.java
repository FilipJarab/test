package senacor.com.evidencesystem.exception;

/**
 * Record from past exception
 * Created by jbonk on 27-Jul-16.
 */
public class RecordFromPastException extends Exception {
    public RecordFromPastException(String message) {
        super(message);
    }
}
