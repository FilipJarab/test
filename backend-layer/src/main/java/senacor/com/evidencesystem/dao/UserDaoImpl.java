package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.UserAlreadyExistsException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


/**
 * User DAO implementation
 * Created by jbonk on 25.05.2016.
 */
@Stateless
public class UserDaoImpl implements UserDao {

    @PersistenceContext(name = "HibernatePersistenceUpdate")
    private EntityManager manager;

    public UserDaoImpl() {
    }

    public UserDaoImpl(EntityManager manager) {
        this.manager = manager;
    }

    /**
     * Gets all users from database
     *
     * @return List of users
     */
    public List<User> getAllUsers() {
        TypedQuery<User> query = manager.createQuery("select u from user u", User.class);
        return query.getResultList();
    }

    /**
     * Gets user with matching email from database
     *
     * @param email Email of user
     * @return User entity, null if no email matches
     */
    public User getUserByEmail(String email) {
        User user;
        try {
            user = (User) manager.createQuery("select u from user u where u.email = ?1")
                    .setParameter(1, email).getSingleResult();
        } catch (NoResultException nre) {
            return User.getNullObject();
        }
        return user;
    }

    /**
     * Inserts new entity into database
     *
     * @param entity Entity to be inserted
     * @throws UserAlreadyExistsException If such user already exists in database
     */
    public void insertEntity(User entity) throws UserAlreadyExistsException {
        if (getUserByEmail(entity.getEmail()).isNull()) {
            manager.persist(entity);
        } else {
            throw new UserAlreadyExistsException("User already exists");
        }
    }

    /**
     * Deletes entity from database
     *
     * @param entity Entity to be removed
     */
    public void deleteEntity(User entity) {
        manager.remove(entity);
    }

    /**
     * Gets count of all users in database
     *
     * @return Count as Long
     */
    public Long getCount() {
        return (Long) manager.createQuery("select count(u) from user u").getSingleResult();
    }

    /**
     * Sets profile of user
     *
     * @param user    The user
     * @param profile Profile to be set
     */
    public void setUsersProfile(User user, Profile profile) {
        User databaseUser = manager.find(User.class, user.getId());
        Profile databaseProfile = null;
        if ((user.getProfile() != null)) {
            databaseProfile = manager.find(Profile.class, user.getProfile().getId());
        }
        if (databaseProfile == null) {
            profile.setUser(databaseUser);
            databaseUser.setProfile(profile);
            manager.merge(databaseUser);
        } else {
            databaseProfile.setUser(databaseUser);
            databaseUser.setProfile(databaseProfile);
            databaseProfile.setTypeOfContract(profile.getTypeOfContract());
            databaseProfile.setDateOfStart(profile.getDateOfStart());
            databaseProfile.setDateOfEnd(profile.getDateOfEnd());
            databaseProfile.setProject(profile.getProject());
            manager.merge(databaseProfile);
            manager.merge(databaseUser);
        }
    }

    /**
     * Sets project for user
     *
     * @param user    The user
     * @param project Project to be set
     */
    public void setUsersProject(User user, Project project) {
        User databaseUser = manager.find(User.class, user.getId());
        Profile databaseProfile = manager.find(Profile.class, databaseUser.getProfile().getId());
        databaseProfile.setProject(project);
        manager.merge(databaseProfile);
    }
}
