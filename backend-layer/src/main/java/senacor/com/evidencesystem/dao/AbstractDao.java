package senacor.com.evidencesystem.dao;

import java.io.Serializable;

/**
 * Abstract DAO
 * Created by jbonk on 25.05.2016.
 */
public interface AbstractDao<T> extends Serializable {

    public abstract void deleteEntity(T entity);

    public abstract Long getCount();

}
