package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.exception.ProjectNameAlreadyExistsException;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.ProjectNameAlreadyExistsException;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.List;

/**
 * Project DAO implementation
 * Created by jbonk on 01.06.2016.
 */
@Stateless
public class ProjectDaoImpl implements ProjectDao {

    @PersistenceContext(name = "HibernatePersistenceUpdate")
    private EntityManager manager;

    public ProjectDaoImpl() {
    }

    public ProjectDaoImpl(EntityManager manager) {
        this.manager = manager;
    }

    /**
     * Gets all projects from database
     *
     * @return List of projects
     */
    public List<Project> getAllProjects() {
        TypedQuery<Project> query =
                manager.createQuery("select p from project p", Project.class);
        return query.getResultList();
    }

    /**
     * Inserts new entity into database
     *
     * @param entity Entity to be inserted
     */
    public void insertEntity(Project entity) {
        manager.persist(entity);
    }

    /**
     * Deletes entity from database
     *
     * @param entity Entity to be removed
     */
    public void deleteEntity(Project entity) {
        manager.remove(entity);
    }

    /**
     * Gets count of all projects in database
     *
     * @return Count as Long
     */
    public Long getCount() {
        return (Long) manager.createQuery("select count(p) from project p").getSingleResult();
    }

    /**
     * Gets project by user entity
     *
     * @param user The user
     * @return Project of the user or null if user does not have any project
     */
    public Project getProjectByUser(User user) {
        Project project;
        try {
            project = (Project) manager.createQuery("select p from project p " +
                    "join p.profile pr join pr.user u where u.id = ?1")
                    .setParameter(1, user.getId()).getSingleResult();
        } catch (NoResultException nre) {
            return Project.getNullObject();
        }
        return project;
    }

    /**
     * Updates project name
     *
     * @param project Project to be updated
     * @param newName New project name
     * @throws ProjectNameAlreadyExistsException If such project name already exists
     */
    public void updateProjectName(Project project, String newName) throws ProjectNameAlreadyExistsException {
        for (Project databaseProject : getAllProjects()) {
            if (databaseProject.getProjectName().equalsIgnoreCase(newName)) {
                throw new ProjectNameAlreadyExistsException("Project name already exists!");
            }
        }
        project.setProjectName(newName);
        manager.merge(project);
    }
}
