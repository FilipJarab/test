package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.User;

import java.io.Serializable;

/**
 * Profile DAO interface
 * Created by jbonk on 10.06.2016.
 */
public interface ProfileDao extends AbstractDao<Profile> {

    void insertEntity(Profile profile);

    Profile getProfileByUser(User user);

}
