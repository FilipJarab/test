package senacor.com.evidencesystem.dao;


import org.apache.poi.ss.usermodel.DataFormat;
import org.joda.time.DateTime;
import org.joda.time.Period;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.RecordFromFutureException;
import senacor.com.evidencesystem.exception.RecordFromPastException;
import sun.util.resources.cldr.kea.CalendarData_kea_CV;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Boolean.*;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

/**
 * Record DAO implementation
 * Created by jbonk on 10.06.2016.
 */
@Stateless
public class RecordDaoImpl implements RecordDao {
    private final static String DATE = "date";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY");

    @PersistenceContext(name = "HibernatePersistenceUpdate")
    private EntityManager manager;

    public RecordDaoImpl() {
    }

    public RecordDaoImpl(EntityManager manager) {
        this.manager = manager;
    }

    /**
     * Inserts new entity into database
     *
     * @param record Entity to be inserted
     */
    public void insertEntity(Record record) {
        manager.persist(record);
        System.out.println(record.getProject());
    }

    /**
     * Deletes entity from database
     *
     * @param entity Entity to be removed
     */
    public void deleteEntity(Record entity) {
        Record record = manager.find(Record.class, entity.getId());
        manager.remove(record);
    }

    /**
     * Gets count of all records in database
     *
     * @return Count as Long
     */
    public Long getCount() {
        return (Long) manager.createQuery("select count(r) from record r").getSingleResult();
    }

    /**
     * Gets user records from atual month
     *
     * @param user Records of this user are returned
     * @param date Records of this month are returned
     * @return List of records
     */
    public List<Record> getRecordsByCurrentMonth(User user, Date date) {

        TypedQuery<Record> query = manager.createQuery("select r from record r join r.user u " +
                "where u.id = ?1 and r.date between ?2 and ?3", Record.class)
                .setParameter(1, user.getId())
                .setParameter(2, getMinMaxDateElement(date, 1))
                .setParameter(3, getMinMaxDateElement(date, 2));
        return query.getResultList();
    }

    /**
     * Gets list of records by emails, project names, months, years and confirmation types
     *
     * @param emails       Records of this users are returned
     * @param projectNames Records with this project names are returned
     * @param months       Records within this months are returned
     * @param years        Records within this years are returned
     * @param states       Records of this type are returned
     * @return List of records
     */
    public List<Record> getRecordsByMultipleProperties(List<String> emails, List<String> projectNames,
                                                       List<Date> months, List<String> years, List<Boolean> states,
                                                       List<Boolean> locks) {
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Record> criteriaQuery = criteriaBuilder.createQuery(Record.class);
        Root<Record> record = criteriaQuery.from(Record.class);
        Join<Record, User> user = record.join("user", JoinType.INNER);
        Join<Record, Project> project = record.join("project", JoinType.INNER);

        List<Predicate> predicates = new ArrayList<>();
        List<Predicate> orEmailsPredicates = new ArrayList<>();
        List<Predicate> orProjectsNamesPredicates = new ArrayList<>();
        List<Predicate> orMonthsPredicates = new ArrayList<>();
        List<Predicate> orYearsPredicates = new ArrayList<>();
        List<Predicate> orStatesPredicates = new ArrayList<>();
        List<Predicate> orLocksPredicates = new ArrayList<>();

        if (isNotEmpty(emails)) {
            for (String email : emails) {
                orEmailsPredicates.add(criteriaBuilder.or(criteriaBuilder.equal(user.get("email"), email)));
            }
            predicates.add(criteriaBuilder.or(orEmailsPredicates.toArray(new Predicate[orEmailsPredicates.size()])));
        }
        if (isNotEmpty(projectNames)) {
            for (String projectName : projectNames) {
                orProjectsNamesPredicates.add(criteriaBuilder.or(criteriaBuilder
                        .equal(project.get("projectName"), projectName)));
            }
            predicates.add(criteriaBuilder.or(orProjectsNamesPredicates.toArray(
                    new Predicate[orProjectsNamesPredicates.size()])));
        }
        if (isNotEmpty(months)) {
            for (Date month : months) {
                orMonthsPredicates.add(criteriaBuilder.or(criteriaBuilder.equal(criteriaBuilder
                        .function("month", Integer.class, record.get(DATE)), getMonthFromDate(month))));
            }
            predicates.add(criteriaBuilder.or(orMonthsPredicates.toArray(new Predicate[orMonthsPredicates.size()])));
        }
        if (isNotEmpty(years)) {
            Calendar calendar1 = Calendar.getInstance();
            Calendar calendar2 = Calendar.getInstance();
            calendar1.set(Calendar.DAY_OF_MONTH, 1);
            calendar1.set(Calendar.HOUR_OF_DAY, 0);
            calendar1.set(Calendar.MINUTE, 0);
            calendar1.set(Calendar.MONTH, 0);
            calendar1.set(Calendar.SECOND, 0);
            calendar2.set(Calendar.DAY_OF_MONTH, 31);
            calendar2.set(Calendar.MONTH, 11);
            calendar2.set(Calendar.HOUR_OF_DAY, 23);
            calendar2.set(Calendar.MINUTE, 59);
            calendar2.set(Calendar.SECOND, 59);
            for (String year : years) {
                calendar1.set(Calendar.YEAR, Integer.parseInt(year));
                calendar2.set(Calendar.YEAR, Integer.parseInt(year));
                Date date = calendar1.getTime();
                Date date2 = calendar2.getTime();
                orYearsPredicates.add(criteriaBuilder.between(record.<Date>get(DATE), date, date2));
            }
            predicates.add(criteriaBuilder.or(orYearsPredicates.toArray(new Predicate[orYearsPredicates.size()])));
        }
        if (isNotEmpty(states)) {
            for (Boolean state : states) {
                orStatesPredicates.add(criteriaBuilder.equal(record.get("authorized"), state));
            }
            predicates.add(criteriaBuilder.or(orStatesPredicates.toArray(new Predicate[orStatesPredicates.size()])));
        }
        if (isNotEmpty(locks)) {
            for (Boolean lock : locks) {
                orLocksPredicates.add(criteriaBuilder.equal(record.get("locked"), lock));
            }
            predicates.add(criteriaBuilder.or(orLocksPredicates.toArray(new Predicate[orLocksPredicates.size()])));
        }
        criteriaQuery.select(record).where(predicates.toArray(new Predicate[predicates.size()]));
        return manager.createQuery(criteriaQuery).getResultList();
    }

    /**
     * Marks records as confirmed
     *
     * @param records Records to be confirmed
     */
    public void confirmRecords(List<Record> records) {
        for (Record record : records) {
            Record databaseRecord = manager.find(Record.class, record.getId());
            databaseRecord.setAuthorized(TRUE);
            manager.merge(databaseRecord);
        }
    }

    /**
     * Marks records as unconfirmed
     *
     * @param records Records to be unconfirmed
     */
    public void unconfirmRecords(List<Record> records) {
        for (Record record : records) {
            Record databaseRecord = manager.find(Record.class, record.getId());
            databaseRecord.setAuthorized(FALSE);
            manager.merge(databaseRecord);
        }
    }

    /**
     * Marks records from current and previous month as locked
     *
     * @param records Records to be locked
     * @throws RecordFromFutureException If any of records is from future
     * @throws RecordFromPastException   If any of records is older than current or previous month
     */
    public void lockRecords(List<Record> records) throws RecordFromFutureException, RecordFromPastException {
        for (Record record : records) {
            if (!isBeforeCurrentDate(record.getDate())) {
                throw new RecordFromFutureException("Can not lock record from future!");
            }
            if (!isCurrentOrPreviousMonth(record.getDate())) {
                throw new RecordFromPastException("Can not lock records older than current or previous month!");
            }
        }
        for (Record record : records) {
            Record databaseRecord = manager.find(Record.class, record.getId());
            databaseRecord.setLocked(TRUE);
            manager.merge(databaseRecord);
        }
    }

    /**
     * Marks records from current and previous month as unlocked and unconfirmed
     *
     * @param records Records to be unlocked and unconfirmed
     */
    public void unlockRecords(List<Record> records) {
        for (Record record : records) {
            if (isBeforeCurrentDate(record.getDate()) && isCurrentOrPreviousMonth(record.getDate())) {
                Record databaseRecord = manager.find(Record.class, record.getId());
                databaseRecord.setAuthorized(FALSE);
                databaseRecord.setLocked(FALSE);
                manager.merge(databaseRecord);
            }
        }
    }

    /**
     * Checks if date is from past
     *
     * @param date Date to be checked
     * @return TRUE if yes, FALSE if it is not
     */
    private Boolean isBeforeCurrentDate(Date date) {
        if (date.before(new Date(System.currentTimeMillis()))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Checks if date is from current or previous month
     *
     * @param date Date to be checked
     * @return TRUE if it is, FALSE if it is not
     */
    private Boolean isCurrentOrPreviousMonth(Date date) {
        Integer currentMonth = getMonthFromDate(new Date(System.currentTimeMillis()));
        if (currentMonth > 1) {
            if (getMonthFromDate(date).equals(currentMonth) || getMonthFromDate(date).equals(currentMonth - 1)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            if (getMonthFromDate(date).equals(currentMonth) || getMonthFromDate(date).equals(currentMonth + 11)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * Calculates total working hours for one record
     *
     * @param record Working hours of this record are calculated
     * @return Total working hours
     */
    public Float calculateTotalHoursForOneRecord(Record record) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        Float totalHours = 0F;
        Integer totalMinutes = 0;
        DateTime timeOfStart = new DateTime(record.getTimeOfStart());
        DateTime timeOfEnd = new DateTime(record.getTimeOfEnd());
        DateTime durationOfPause = new DateTime(record.getDurationOfPause());
        Period workingTime = new Period(timeOfStart, timeOfEnd);
        totalHours += workingTime.getHours();
        totalMinutes += workingTime.getMinutes();
        DateTime zero = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        Period pauseTime = new Period(zero, durationOfPause);
        totalHours -= pauseTime.getHours();
        totalMinutes -= pauseTime.getMinutes();
        float totalTime = totalHours + (float) totalMinutes / 60;
        String formattedFloat = decimalFormat.format(totalTime);
        formattedFloat = formattedFloat.replace(",", ".");
        return Float.parseFloat(formattedFloat);
    }

    /**
     * Calculates total working hours for desired month
     *
     * @param user Working hours of this user are calculated
     * @param date Working hours of this month are calculated
     * @return Total working hours
     */
    public Float calculateTotalHours(User user, Date date) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        List<Record> records = getRecordsByCurrentMonth(user, date);
        Float totalHours = 0F;
        for (Record record : records) {
            totalHours += calculateTotalHoursForOneRecord(record);
        }
        String formattedFloat = decimalFormat.format(totalHours);
        formattedFloat = formattedFloat.replace(",", ".");
        return Float.parseFloat(formattedFloat);
    }

    public void updateRecordsPauseDuration(Record record, Date durationOfPause) {
        Record databaseRecord = manager.find(Record.class, record.getId());
        databaseRecord.setDurationOfPause(durationOfPause);
        manager.merge(databaseRecord);
    }

    public void updateRecordsTimeOfStart(Record record, Date timeOfStart) {
        Record databaseRecord = manager.find(Record.class, record.getId());
        databaseRecord.setTimeOfStart(timeOfStart);
        manager.merge(databaseRecord);
    }

    public void updateRecordsTimeOfEnd(Record record, Date timeOfEnd) {
        Record databaseRecord = manager.find(Record.class, record.getId());
        databaseRecord.setTimeOfEnd(timeOfEnd);
        manager.merge(databaseRecord);
    }

    public void updateRecordsTypeOfWork(Record record, String typeOfWork) {
        Record databaseRecord = manager.find(Record.class, record.getId());
        databaseRecord.setTypeOfWork(typeOfWork);
        manager.merge(databaseRecord);
    }

    /**
     * Gets min or max date of desired date element
     *
     * @param date Min or max element of this date is returned
     * @param type 1 - min day of month, 2 - max day of month, 3 - min day of year, 4 - max day of year
     * @return Resulting date
     */
    private static Date getMinMaxDateElement(Date date, Integer type) {
        Date resultDate = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        switch (type) {
            case 1:
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getMinimum(Calendar.DAY_OF_MONTH));
                resultDate = calendar.getTime();
                break;
            case 2:
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getMaximum(Calendar.DAY_OF_MONTH));
                resultDate = calendar.getTime();
                break;
            case 3:
                calendar.set(Calendar.DAY_OF_YEAR, calendar.getMinimum(Calendar.DAY_OF_YEAR));
                resultDate = calendar.getTime();
                break;
            case 4:
                calendar.set(Calendar.DAY_OF_YEAR, calendar.getMaximum(Calendar.DAY_OF_YEAR));
                resultDate = calendar.getTime();
                break;
            default:
                break;
        }
        return resultDate;
    }

    /**
     * Gets number of month from date object
     *
     * @param date Month of this date is returned
     * @return The number of month
     */
    private Integer getMonthFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // + 1 because months start at index 0
        return calendar.get(Calendar.MONTH) + 1;
    }

    //TODO
    public List<Record> getAllRecords() {
        TypedQuery<Record> query =
                manager.createQuery("select p from record p", Record.class);
        return query.getResultList();
    }
}
