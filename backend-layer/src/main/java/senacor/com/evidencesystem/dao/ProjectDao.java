package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.ProjectNameAlreadyExistsException;

import java.io.Serializable;
import java.util.List;

/**
 * Project DAO interface
 * Created by jbonk on 01.06.2016.
 */
public interface ProjectDao extends AbstractDao<Project> {

    void insertEntity(Project entity);

    List<Project> getAllProjects();

    Project getProjectByUser(User user);

    void updateProjectName(Project project, String newName) throws ProjectNameAlreadyExistsException;

}
