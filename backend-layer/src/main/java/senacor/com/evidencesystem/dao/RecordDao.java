package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.RecordFromFutureException;
import senacor.com.evidencesystem.exception.RecordFromPastException;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Record DAO interface
 * Created by jbonk on 10.06.2016.
 */
public interface RecordDao extends AbstractDao<Record> {

    void insertEntity(Record record);

    List<Record> getRecordsByCurrentMonth(User user, Date date);

    List<Record> getRecordsByMultipleProperties(List<String> emails, List<String> projectNames, List<Date> months,
                                                List<String> years, List<Boolean> states, List<Boolean> locks);

    void confirmRecords(List<Record> records);

    void unconfirmRecords(List<Record> records);

    void lockRecords(List<Record> records) throws RecordFromFutureException, RecordFromPastException;

    void unlockRecords(List<Record> records);

    void deleteEntity(Record entity);

    void updateRecordsPauseDuration(Record record, Date durationOfPause);

    void updateRecordsTimeOfStart(Record record, Date timeOfStart);

    void updateRecordsTimeOfEnd(Record record, Date timeOfEnd);

    void updateRecordsTypeOfWork(Record record, String typeOfWork);

    Float calculateTotalHoursForOneRecord(Record record);

    Float calculateTotalHours(User user, Date date);

    List<Record> getAllRecords();
}
