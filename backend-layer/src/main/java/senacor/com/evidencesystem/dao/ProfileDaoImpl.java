package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.User;

import javax.ejb.Stateless;
import javax.persistence.*;

/**
 * Profile DAO implementation
 * Created by jbonk on 10.06.2016.
 */
@Stateless
public class ProfileDaoImpl implements ProfileDao {

    @PersistenceContext(name = "HibernatePersistenceUpdate")
    private EntityManager manager;

    public ProfileDaoImpl() {
    }

    public ProfileDaoImpl(EntityManager manager) {
        this.manager = manager;
    }

    /**
     * Inserts new entity into database
     *
     * @param profile Entity to be inserted
     */
    public void insertEntity(Profile profile) {
        manager.persist(profile);
    }

    /**
     * Deletes entity from database
     *
     * @param entity Entity to be removed
     */
    public void deleteEntity(Profile entity) {
        manager.remove(entity);
    }

    /**
     * Gets count of all profiles in database
     *
     * @return Count as Long
     */
    public Long getCount() {
        return (Long) manager.createQuery("select count(p) from profile p").getSingleResult();
    }

    /**
     * Gets profile by user
     *
     * @param user The user
     * @return Profile connected with user or null if there is not profile connected with user
     */
    public Profile getProfileByUser(User user) {
        Profile profile;
        try {
            profile = (Profile) manager.createQuery("select p from profile p join p.user u where u.id = ?1")
                    .setParameter(1, user.getId()).getSingleResult();
        } catch (NoResultException nre) {
            return Profile.getNullObject();
        }
        return profile;
    }
}
