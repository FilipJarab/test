package senacor.com.evidencesystem.dao;

import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.UserAlreadyExistsException;

import java.io.Serializable;
import java.util.List;

/**
 * User DAO interface
 * Created by jbonk on 25.05.2016.
 */
public interface UserDao extends AbstractDao<User> {

    void insertEntity(User entity) throws UserAlreadyExistsException;

    List<User> getAllUsers();

    User getUserByEmail(String email);

    void setUsersProfile(User user, Profile profile);

    void setUsersProject(User user, Project project);

}
