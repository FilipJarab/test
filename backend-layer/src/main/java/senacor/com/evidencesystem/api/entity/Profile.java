package senacor.com.evidencesystem.api.entity;

import senacor.com.evidencesystem.api.Nullable;
import senacor.com.evidencesystem.api.enumeration.TypeOfContract;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Profile entity
 * Created by jbonk on 02.06.2016.
 */
@Entity(name = "profile")
public class Profile implements Nullable {
    private static final String TYPE_OF_CONTRACT = "type_of_contract";
    private static final String DATE_OF_START = "date_of_start";
    private static final String DATE_OF_END = "date_of_end";

    @Transient
    private boolean isNull;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = TYPE_OF_CONTRACT)
    @Enumerated(EnumType.STRING)
    private TypeOfContract typeOfContract;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = DATE_OF_START)
    private Date dateOfStart;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = DATE_OF_END)
    private Date dateOfEnd;

    @OneToOne
    private User user;

    @ManyToOne
    private Project project;

    public Profile() {
        this.isNull = false;
    }

    public TypeOfContract getTypeOfContract() {
        return typeOfContract;
    }

    public void setTypeOfContract(TypeOfContract typeOfContract) {
        this.typeOfContract = typeOfContract;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateOfStart() {
        return dateOfStart;
    }

    public void setDateOfStart(Date dateOfStart) {
        this.dateOfStart = dateOfStart;
    }

    public Date getDateOfEnd() {
        return dateOfEnd;
    }

    public void setDateOfEnd(Date dateOfEnd) {
        this.dateOfEnd = dateOfEnd;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setNull(boolean isNull) {
        this.isNull = isNull;
    }

    @Override
    public boolean isNull() {
        return this.isNull;
    }

    //TODO can go to utility, should be just one utility implementation with class parameter input type
    public static Profile getNullObject() {
        Profile profile = new Profile();
        profile.setNull(true);
        return profile;
    }
}