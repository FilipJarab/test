package senacor.com.evidencesystem.api.entity;

import senacor.com.evidencesystem.api.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Project entity
 * Created by jbonk on 01.06.2016.
 */
@Entity(name = "project")
public class Project implements Nullable {
    private static final String PROJECT_NAME = "project_name";

    @Transient
    private boolean isNull;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 60, message = "wrong project name size")
    @Column(nullable = false, name = PROJECT_NAME)
    private String projectName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
    private List<Profile> profile;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
    private List<Record> records;

    public Project() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public List<Profile> getProfile() {
        return profile;
    }

    public void setProfile(List<Profile> profile) {
        this.profile = profile;
    }

    public void setNull(boolean isNull) {
        this.isNull = isNull;
    }

    @Override
    public boolean isNull() {
        return this.isNull;
    }

    //TODO can go to utility, should be just one utility implementation with class parameter input type
    public static Project getNullObject() {
        Project project = new Project();
        project.setNull(true);
        return project;
    }
}
