package senacor.com.evidencesystem.api.enumeration;

/**
 * Type of contract enumeration
 * Created by jbonk on 27.06.2016.
 */
public enum TypeOfContract {
    PART_TIME("Part Time"),
    FULL_TIME("Full Time");

    String name;

    TypeOfContract(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
