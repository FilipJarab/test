package senacor.com.evidencesystem.api;

/**
 * Nullable interface
 * Created by fjarab on 05.08.2016.
 */
public interface Nullable {

    boolean isNull();

}
