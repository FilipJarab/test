package senacor.com.evidencesystem.api.enumeration;

/**
 * Rank enumeration
 * Created by jbonk on 27.06.2016.
 */
public enum Rank {
    STUDENT("Student"),
    ADMIN("Office Manager");

    private String name;

    Rank(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
