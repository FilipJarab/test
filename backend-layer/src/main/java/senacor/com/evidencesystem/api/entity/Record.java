package senacor.com.evidencesystem.api.entity;

import senacor.com.evidencesystem.api.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Record entity
 * Created by jbonk on 02.06.2016.
 */
@Entity(name = "record")
public class Record implements Nullable {
    private static final String TIME_OF_START = "time_of_start";
    private static final String TIME_OF_END = "time_of_end";
    private static final String DURATION_OF_PAUSE = "duration_of_pause";
    private static final String TYPE_OF_WORK = "type_of_work";

    @Transient
    private boolean isNull;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Temporal(TemporalType.TIME)
    @Column(nullable = false, name = TIME_OF_START)
    private Date timeOfStart;

    @NotNull
    @Temporal(TemporalType.TIME)
    @Column(nullable = false, name = TIME_OF_END)
    private Date timeOfEnd;

    @NotNull
    @Temporal(TemporalType.TIME)
    @Column(nullable = false, name = DURATION_OF_PAUSE)
    private Date durationOfPause;

    @NotNull
    @Size(min = 1, max = 50, message = "wrong type of work size")
    @Column(nullable = false, name = TYPE_OF_WORK)
    private String typeOfWork;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date date;

    @Column(nullable = false)
    private boolean authorized;

    @Column(nullable = false)
    private boolean locked;

    @ManyToOne(fetch = FetchType.EAGER)
    private Project project;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    public Record() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimeOfStart() {
        return timeOfStart;
    }

    public void setTimeOfStart(Date timeOfStart) {
        this.timeOfStart = timeOfStart;
    }

    public Date getTimeOfEnd() {
        return timeOfEnd;
    }

    public void setTimeOfEnd(Date timeOfEnd) {
        this.timeOfEnd = timeOfEnd;
    }

    public String getTypeOfWork() {
        return typeOfWork;
    }

    public void setTypeOfWork(String typeOfWork) {
        this.typeOfWork = typeOfWork;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDurationOfPause() {
        return durationOfPause;
    }

    public void setDurationOfPause(Date durationOfPause) {
        this.durationOfPause = durationOfPause;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void setNull(boolean isNull) {
        this.isNull = isNull;
    }

    @Override
    public boolean isNull() {
        return this.isNull;
    }

    //TODO can go to utility, should be just one utility implementation with class parameter input type
    public static Record getNullObject() {
        Record record = new Record();
        record.setNull(true);
        return record;
    }
}
