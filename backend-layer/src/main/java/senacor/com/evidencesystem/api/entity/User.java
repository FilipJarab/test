package senacor.com.evidencesystem.api.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import senacor.com.evidencesystem.api.Nullable;
import senacor.com.evidencesystem.api.enumeration.Rank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * User Entity
 * Created by jbonk on 25.05.2016.
 */
@Entity(name = "user")
public class User implements Nullable {
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";

    @Transient
    private boolean isNull;

    @Override
    public String toString() {
        return "User{" +
                "isNull=" + isNull +
                ", id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", rank=" + rank +
                ", profile=" + profile +
                ", records=" + records +
                '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 60, message = "wrong email size")
    @Pattern(regexp = "[a-zA-Z]+[.-][a-zA-z]+@senacor.com", message = "wrong email pattern")
    @Column(nullable = false)
    private String email;

    @NotNull
    @Size(min = 1, max = 200, message = "wrong password size")
    @Column(nullable = false)
    private String password;

    @Size(min = 1, max = 30, message = "wrong first name size")
    @Column(nullable = false, name = FIRST_NAME)
    private String firstName;

    @Size(min = 1, max = 30, message = "wrong last name size")
    @Column(nullable = false, name = LAST_NAME)
    private String lastName;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Rank rank;

    @OneToOne(mappedBy = "user", orphanRemoval = true)
    @Cascade(CascadeType.ALL)
    private Profile profile;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private List<Record> records;

    public User() {
    }

    public User(String emailAddress, String password, String firstName, String lastName) {
        this.email = emailAddress;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(String emailAddress, String password) {
        this.email = emailAddress;
        this.password = password;
        this.setUserName();
        this.setUserRank();
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private void setUserName() {
        if (this.email != null) {
            String[] address = this.email.split("[\\.-]");
            this.setFirstName(address[0]);
            this.setLastName(address[1].substring(0, address[1].indexOf('@')));
        }
    }

    private void setUserRank() {
        if (this.getFirstName() == null || this.getLastName() == null) {
            return;
        }
        String officerMail = System.getenv("OFFICER");
        if (email.equals(officerMail)) {
            this.setRank(Rank.ADMIN);

        } else {
            this.setRank(Rank.STUDENT);
        }
    }

    @Override
    public boolean equals(Object object) {
        return ((User) object).getEmail().equals(this.email);

    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setNull(boolean isNull) {
        this.isNull = isNull;
    }

    @Override
    public boolean isNull() {
        return this.isNull;
    }

    //TODO can go to utility, should be just one utility implementation with class parameter input type
    public static User getNullObject() {
        User user = new User();
        user.setNull(true);
        return user;
    }
}
