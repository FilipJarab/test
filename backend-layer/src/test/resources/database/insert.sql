INSERT  INTO user (email, password, first_name, last_name, rank) VALUES ('Adam.Novak@senacor.com', 'passwd', 'Adam', 'Novak', 'STUDENT');
INSERT  INTO user (email, password, first_name, last_name, rank) VALUES ('Jozef.Hrozny@senacor.com', 'heslo', 'Jozef', 'Hrozny', 'STUDENT');
INSERT  INTO user (email, password, first_name, last_name, rank) VALUES ('Peter.Nekonecny@senacor.com', 'asdf', 'Peter', 'Nekonecny', 'STUDENT');

INSERT INTO project (project_name) VALUES ('The Project');
INSERT INTO project (project_name) VALUES ('Senacor Project');
INSERT INTO project (project_name) VALUES ('Evidence System');

INSERT INTO profile (type_of_contract, date_of_start, date_of_end, user_id, project_id) VALUES ('PART_TIME', CURRENT_TIMESTAMP, DATEADD('DAY', 365, CURRENT_TIMESTAMP), 1, 1);
INSERT INTO profile (type_of_contract, date_of_start, date_of_end, user_id, project_id) VALUES ('PART_TIME', CURRENT_TIMESTAMP, DATEADD('DAY', 365, CURRENT_TIMESTAMP), 2, 1);
INSERT INTO profile (type_of_contract, date_of_start, date_of_end, user_id, project_id) VALUES ('FULL_TIME', CURRENT_TIMESTAMP, DATEADD('DAY', 365, CURRENT_TIMESTAMP), 3, 2);

INSERT INTO record (time_of_start, time_of_end, duration_of_pause, type_of_work, date, authorized, locked, user_id, project_id) VALUES (CURRENT_TIMESTAMP, DATEADD('HOUR', 8, CURRENT_TIMESTAMP), '00:30:00', 'Programming', CURRENT_TIMESTAMP, FALSE, FALSE, 1, 1);
INSERT INTO record (time_of_start, time_of_end, duration_of_pause, type_of_work, date, authorized, locked, user_id, project_id) VALUES (CURRENT_TIMESTAMP, DATEADD('HOUR', 8, CURRENT_TIMESTAMP), '00:30:00', 'Programming', CURRENT_TIMESTAMP, FALSE, FALSE, 2, 1);
INSERT INTO record (time_of_start, time_of_end, duration_of_pause, type_of_work, date, authorized, locked, user_id, project_id) VALUES (CURRENT_TIMESTAMP, DATEADD('HOUR', 8, CURRENT_TIMESTAMP), '00:30:00', 'Programming', CURRENT_TIMESTAMP, FALSE, FALSE, 3, 1);