package senacor.com.evidencesystem.helper;

import org.apache.commons.lang3.time.DateUtils;
import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.api.enumeration.Rank;
import senacor.com.evidencesystem.api.enumeration.TypeOfContract;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Test helper class
 * Created by jbonk on 13.06.2016.
 */
public class TestHelper {

    /**
     * Creates user for tests
     *
     * @return test user
     */
    public User createTestUser(String string) {
        User user = new User();
        user.setEmail(string + "." + string + "@senacor.com");
        user.setPassword("tests");
        user.setFirstName(string);
        user.setLastName(string);
        user.setRank(Rank.STUDENT);
        return user;
    }

    /**
     * Creates project for tests
     *
     * @return test project
     */
    public Project createTestProject() {
        Project project = new Project();
        project.setProjectName("Test");
        return project;
    }

    /**
     * Creates profile for tests
     *
     * @return test profile
     */
    public Profile createTestProfile() {
        Profile profile = new Profile();
        profile.setTypeOfContract(TypeOfContract.PART_TIME);
        profile.setDateOfStart(formatDate("01. 06. 2016"));
        profile.setDateOfEnd(formatDate("24. 12. 2020"));
        return profile;
    }

    /**
     * Creates record for tests
     *
     * @return test record
     */
    public Record createTestRecord() {
        Record record = new Record();
        record.setTypeOfWork("Test");
        record.setTimeOfStart(formatDateTime("01. 01. 2016, 09:00"));
        record.setTimeOfEnd(formatDateTime("01. 01. 2016, 17:00"));
        //30 minutes = 1 800 000 millis
        Time time = new Time(1800000);
        Date date = DateUtils.addHours(time, -1);
        record.setDurationOfPause(date);
        record.setDate(new Date(System.currentTimeMillis()));
        return record;
    }

    /**
     * Initializes the entity manager
     *
     * @return the entity manager
     */
    public EntityManager initEntityManager() {
        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("HibernatePersistenceTest");
        return managerFactory.createEntityManager();
    }

    /**
     * Initializes the validator
     *
     * @return Initialized validator
     */
    public Validator initValidator() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }

    /**
     * Formats string to date
     *
     * @param dateString date as String
     * @return Date object
     */
    private static Date formatDate(String dateString) {
        Date date = null;
        DateFormat format = new SimpleDateFormat("dd. MM. yyyy");
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Formats string to date and time
     *
     * @param dateString date as String
     * @return Date object
     */
    private static Date formatDateTime(String dateString) {
        Date date = null;
        DateFormat format = new SimpleDateFormat("dd. MM. yyyy, HH:mm");
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
