package senacor.com.evidencesystem.dao;

import org.junit.*;
import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.UserAlreadyExistsException;
import senacor.com.evidencesystem.helper.TestHelper;

import javax.persistence.EntityManager;

import java.util.List;

import static org.junit.Assert.*;

/**
 * User DAO test
 * Created by jbonk on 02.06.2016.
 */
public class UserDaoImplTest {
    private TestHelper testHelper;
    private UserDaoImpl userDao;
    private ProjectDaoImpl projectDao;
    private EntityManager manager;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        userDao = new UserDaoImpl(manager);
        projectDao = new ProjectDaoImpl(manager);
    }

    @After
    public void close() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void getCountTest() {
        Long count = userDao.getCount();

        //count should be 3
        assertEquals(new Long(3), count);
    }

    @Test
    public void insertEntityTest() throws UserAlreadyExistsException {
        User user = testHelper.createTestUser("Aa");
        Long beforeInsertCount = userDao.getCount();
        manager.getTransaction().begin();
        userDao.insertEntity(user);
        manager.getTransaction().commit();
        Long afterInsertCount = userDao.getCount();

        assertEquals(new Long(beforeInsertCount + 1), afterInsertCount);
    }

    @Test
    public void deleteEntityTest() throws UserAlreadyExistsException {
        User user = testHelper.createTestUser("Bb");
        manager.getTransaction().begin();
        userDao.insertEntity(user);
        manager.getTransaction().commit();
        Long beforeDeleteCount = userDao.getCount();
        manager.getTransaction().begin();
        userDao.deleteEntity(user);
        manager.getTransaction().commit();
        Long afterDeleteCount = userDao.getCount();

        assertEquals(new Long(beforeDeleteCount - 1), afterDeleteCount);
    }

    @Test
    public void getAllUsersTest() {
        List<User> users = userDao.getAllUsers();

        //should return 3 users
        assertEquals(3, users.size());
    }

    @Test
    public void userAlreadyExistsTest() {
        User user = testHelper.createTestUser("Cc");
        Boolean userExists = false;
        try {
            manager.getTransaction().begin();
            userDao.insertEntity(user);
            userDao.insertEntity(user);
        } catch (UserAlreadyExistsException e) {
            userExists = true;
        } finally {
            manager.getTransaction().commit();
        }
        //userExists should be true
        assertTrue(userExists);
    }

    @Test
    public void getUserByEmailTest() {
        User user = userDao.getUserByEmail("Adam.Novak@senacor.com");
        assertFalse(user.isNull());

        user = userDao.getUserByEmail("Test.Test@senacor.com");

        //user should be instance of NullUser
        assertTrue(user.isNull());
    }

    @Test
    public void setUsersProfileTest() throws UserAlreadyExistsException {
        User user = testHelper.createTestUser("Dd");
        Profile profile = testHelper.createTestProfile();
        manager.getTransaction().begin();
        userDao.insertEntity(user);
        userDao.setUsersProfile(user, profile);
        manager.getTransaction().commit();

        //users ID and profiles assigned user ID should be same
        assertEquals(user.getId(), profile.getUser().getId());
    }

    @Test
    public void setUsersProjectTest() throws UserAlreadyExistsException {
        User user = testHelper.createTestUser("Ee");
        Profile profile = testHelper.createTestProfile();
        Project project = testHelper.createTestProject();
        manager.getTransaction().begin();
        userDao.insertEntity(user);
        projectDao.insertEntity(project);
        userDao.setUsersProfile(user, profile);
        userDao.setUsersProject(user, project);
        manager.getTransaction().commit();

        //projects ID and projects assigned to user ID should be same
        assertEquals(project.getId(), user.getProfile().getProject().getId());
    }
}