package senacor.com.evidencesystem.dao;

import org.junit.*;
import senacor.com.evidencesystem.api.entity.Profile;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.helper.TestHelper;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;

/**
 * Profile DAO test
 * Created by jbonk on 13.06.2016.
 */
public class ProfileDaoImplTest {
    private ProfileDaoImpl profileDao;
    private UserDaoImpl userDao;
    private TestHelper testHelper;
    private EntityManager manager;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        profileDao = new ProfileDaoImpl(manager);
        userDao = new UserDaoImpl(manager);
    }

    @After
    public void close() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void getCountTest() {
        Long count = profileDao.getCount();

        //count should be 3
        assertEquals(new Long(3), count);
    }

    @Test
    public void insertEntityTest() {
        Profile profile = testHelper.createTestProfile();
        Long beforeInsertCount = profileDao.getCount();
        manager.getTransaction().begin();
        profileDao.insertEntity(profile);
        manager.getTransaction().commit();
        Long afterInsertCount = profileDao.getCount();

        assertEquals(new Long(beforeInsertCount + 1), afterInsertCount);
    }

    @Test
    public void deleteEntityTest() {
        Profile profile = testHelper.createTestProfile();
        manager.getTransaction().begin();
        profileDao.insertEntity(profile);
        manager.getTransaction().commit();
        Long beforedeleteCount = profileDao.getCount();
        manager.getTransaction().begin();
        profileDao.deleteEntity(profile);
        manager.getTransaction().commit();
        Long afterDeleteCount = profileDao.getCount();

        assertEquals(new Long(beforedeleteCount - 1), afterDeleteCount);
    }

    @Test
    public void getProfileByUserTest() {
        User user = userDao.getUserByEmail("Adam.Novak@senacor.com");
        Profile userProfile = profileDao.getProfileByUser(user);

        //users ID and profiles user ID should be same
        assertEquals(user.getProfile().getId(), userProfile.getId());
    }
}
