package senacor.com.evidencesystem.dao;

import org.junit.*;
import senacor.com.evidencesystem.api.entity.Project;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.ProjectNameAlreadyExistsException;
import senacor.com.evidencesystem.helper.TestHelper;


import javax.persistence.EntityManager;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static java.lang.Boolean.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertTrue;
import static java.lang.Boolean.*;

/**
 * Project DAO test
 * Created by jbonk on 13.06.2016.
 */
public class ProjectDaoImplTest {
    private ProjectDaoImpl projectDao;
    private UserDaoImpl userDao;
    private TestHelper testHelper;
    private EntityManager manager;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        projectDao = new ProjectDaoImpl(manager);
        userDao = new UserDaoImpl(manager);
    }

    @After
    public void close() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void getCountTest() {
        Long count = projectDao.getCount();

        //count should be 3
        assertEquals(new Long(3), count);
    }

    @Test
    public void insertEntityTest() {
        Project project = testHelper.createTestProject();
        Long beforeInsertCount = projectDao.getCount();
        manager.getTransaction().begin();
        projectDao.insertEntity(project);
        manager.getTransaction().commit();
        Long afterInsertCount = projectDao.getCount();

        assertEquals(new Long(beforeInsertCount + 1), afterInsertCount);
    }

    @Test
    public void deleteEntityTest() {
        Project project = testHelper.createTestProject();
        manager.getTransaction().begin();
        projectDao.insertEntity(project);
        manager.getTransaction().commit();
        Long beforeDeleteCount = projectDao.getCount();
        manager.getTransaction().begin();
        projectDao.deleteEntity(project);
        manager.getTransaction().commit();
        Long afterDeleteCount = projectDao.getCount();

        assertEquals(new Long(beforeDeleteCount - 1), afterDeleteCount);
    }

    @Test
    public void getAllProjectsTest() {
        List<Project> projects = projectDao.getAllProjects();

        //should return 3 projects
        assertEquals(3, projects.size());
    }

    @Test
    public void getProjectByUserTest() {
        User user = userDao.getUserByEmail("Adam.Novak@senacor.com");
        Project userProject = projectDao.getProjectByUser(user);

        //projects name should be The Project
        assertEquals("The Project", userProject.getProjectName());
    }

    @Test
    public void updateProjectNameTest() throws ProjectNameAlreadyExistsException {
        Project project = testHelper.createTestProject();
        manager.getTransaction().begin();
        projectDao.insertEntity(project);
        try {
            projectDao.updateProjectName(project, "New Name");
        } catch (ProjectNameAlreadyExistsException e) {
            e.printStackTrace();
        }
        manager.getTransaction().commit();

        //projects name should be "New Name"
        assertEquals("New Name", project.getProjectName());
    }

    @Test
    public void updateProjectNameToExistingNameTest() {
        Boolean nameExists = FALSE;
        Project project = testHelper.createTestProject();
        manager.getTransaction().begin();
        projectDao.insertEntity(project);
        try {
            projectDao.updateProjectName(project, "Test");
        } catch (ProjectNameAlreadyExistsException e) {
            nameExists = TRUE;
        } finally {
            manager.getTransaction().commit();
        }

        //nameExists should be true
        assertTrue(nameExists);
    }
}
