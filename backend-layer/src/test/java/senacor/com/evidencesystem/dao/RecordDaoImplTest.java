package senacor.com.evidencesystem.dao;

import org.junit.*;
import senacor.com.evidencesystem.api.entity.Record;
import senacor.com.evidencesystem.api.entity.User;
import senacor.com.evidencesystem.exception.RecordFromFutureException;
import senacor.com.evidencesystem.exception.RecordFromPastException;
import senacor.com.evidencesystem.helper.TestHelper;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.lang.Boolean.*;
import static org.junit.Assert.*;

/**
 * Record DAO test
 * Created by jbonk on 13.06.2016.
 */
public class RecordDaoImplTest {
    private TestHelper testHelper;
    private RecordDaoImpl recordDao;
    private UserDaoImpl userDao;
    private EntityManager manager;
    private Calendar calendar = Calendar.getInstance();

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        userDao = new UserDaoImpl(manager);
        recordDao = new RecordDaoImpl(manager);
    }

    @After
    public void close() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void getCountTest() {
        Long count = recordDao.getCount();

        //count should be 3
        assertEquals(new Long(3), count);
    }

    @Test
    public void insertEntityTest() {
        Record record = testHelper.createTestRecord();
        Long beforeInsertCount = recordDao.getCount();
        manager.getTransaction().begin();
        recordDao.insertEntity(record);
        manager.getTransaction().commit();
        Long afterInsertCount = recordDao.getCount();

        assertEquals(new Long(beforeInsertCount + 1), afterInsertCount);
    }

    @Test
    public void deleteEntityTest() {
        Record record = testHelper.createTestRecord();
        manager.getTransaction().begin();
        recordDao.insertEntity(record);
        Long beforeDeleteCount = recordDao.getCount();
        recordDao.deleteEntity(record);
        manager.getTransaction().commit();
        Long afterDeleteCount = recordDao.getCount();

        assertEquals(new Long(beforeDeleteCount - 1), afterDeleteCount);
    }

    @Test
    public void getRecordsByCurrentMonthTest() {
        UserDaoImpl userDao = new UserDaoImpl(manager);
        User user = userDao.getUserByEmail("Adam.Novak@senacor.com");
        Date date = new Date(System.currentTimeMillis());
        List<Record> records = recordDao.getRecordsByCurrentMonth(user, date);

        //records should contain 1 record
        assertEquals(1, records.size());
    }

    @Test
    public void getRecordsByMultiplePropertiesTest() {
        List<Record> records;
        List<String> emails = new ArrayList<>();
        List<String> projectNames = new ArrayList<>();
        List<Date> months = new ArrayList<>();
        List<String> years = new ArrayList<>();
        List<Boolean> states = new ArrayList<>();
        List<Boolean> locks = new ArrayList<>();
        emails.add("Adam.Novak@senacor.com");
        emails.add("Jozef.Hrozny@senacor.com");
        projectNames.add("The Project");
        months.add(new Date(System.currentTimeMillis()));
        years.add(String.valueOf(calendar.get(Calendar.YEAR)));
        states.add(FALSE);
        locks.add(FALSE);
        records = recordDao.getRecordsByMultipleProperties(emails, projectNames, months, years, states, locks);

        //should return 2 records
        assertEquals(2, records.size());

        states.clear();
        states.add(TRUE);
        records = recordDao.getRecordsByMultipleProperties(emails, projectNames, months, years, states, locks);

        //should return 0 records
        assertEquals(0, records.size());
    }

    @Test
    public void confirmRecordsTest() {
        List<Record> records = new ArrayList<>();
        records.add(testHelper.createTestRecord());
        records.add(testHelper.createTestRecord());
        manager.getTransaction().begin();
        recordDao.insertEntity(records.get(0));
        recordDao.insertEntity(records.get(1));
        recordDao.confirmRecords(records);
        manager.getTransaction().commit();

        //both records should be authorized
        assertTrue(records.get(0).isAuthorized());
        assertTrue(records.get(1).isAuthorized());
    }

    @Test
    public void unconfirmRecordsTest() {
        List<Record> records = new ArrayList<>();
        records.add(testHelper.createTestRecord());
        records.add(testHelper.createTestRecord());
        records.get(0).setAuthorized(TRUE);
        records.get(1).setAuthorized(TRUE);
        manager.getTransaction().begin();
        recordDao.insertEntity(records.get(0));
        recordDao.insertEntity(records.get(1));
        recordDao.unconfirmRecords(records);
        manager.getTransaction().commit();

        //both records should be unauthorized
        assertFalse(records.get(0).isAuthorized());
        assertFalse(records.get(1).isAuthorized());
    }

    @Test(expected = RecordFromFutureException.class)
    public void lockRecordFromFutureTest() throws RecordFromFutureException, RecordFromPastException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.add(Calendar.DATE, 1);

        List<Record> records = new ArrayList<>();
        Record record = testHelper.createTestRecord();
        record.setDate(calendar.getTime());
        manager.getTransaction().begin();
        recordDao.insertEntity(record);
        manager.getTransaction().commit();
        records.add(record);

        //should produce RecordFromFutureException
        recordDao.lockRecords(records);
    }

    @Test(expected = RecordFromPastException.class)
    public void lockRecordFromPastTest() throws RecordFromFutureException, RecordFromPastException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.add(Calendar.MONTH, -2);

        List<Record> records = new ArrayList<>();
        Record record = testHelper.createTestRecord();
        record.setDate(calendar.getTime());
        manager.getTransaction().begin();
        recordDao.insertEntity(record);
        manager.getTransaction().commit();
        records.add(record);

        //should produce RecordFromFutureException
        recordDao.lockRecords(records);
    }

    @Test
    public void unlockRecordsTest() {
        List<Record> records = new ArrayList<>();
        Record record = testHelper.createTestRecord();
        Record record2 = testHelper.createTestRecord();
        record.setLocked(TRUE);
        record2.setLocked(TRUE);
        records.add(record);
        records.add(record2);

        manager.getTransaction().begin();
        recordDao.insertEntity(records.get(0));
        recordDao.insertEntity(records.get(1));
        manager.getTransaction().commit();
        recordDao.unlockRecords(records);

        //records should be unlocked
        assertFalse(records.get(0).isLocked());
        assertFalse(records.get(1).isLocked());
    }

    @Test
    public void calculateTotalHoursForOneRecordTest() {
        Record record = testHelper.createTestRecord();
        Float totalHours = recordDao.calculateTotalHoursForOneRecord(record);

        //total hours should be 7.5
        assertEquals(new Float(7.5), totalHours);
    }

    @Test
    public void calculateTotalHoursAdditionTest() {
        User user = userDao.getUserByEmail("Adam.Novak@senacor.com");
        Float totalHours = recordDao.calculateTotalHours(user, new Date(System.currentTimeMillis()));

        //total hours should be 7.5
        assertEquals(new Float(7.5), totalHours);

        Record record = testHelper.createTestRecord();
        record.setUser(user);
        manager.getTransaction().begin();
        recordDao.insertEntity(record);
        manager.getTransaction().commit();
        totalHours = recordDao.calculateTotalHours(user, new Date(System.currentTimeMillis()));

        //total hours should be 15
        assertEquals(new Float(15), totalHours);
    }

    @Test
    public void calculateTotalHoursSubtractionTest() {
        User user = userDao.getUserByEmail("Adam.Novak@senacor.com");
        Float totalHours = recordDao.calculateTotalHours(user, new Date(System.currentTimeMillis()));

        //total hours should be 7.5
        assertEquals(new Float(7.5), totalHours);

        Record record = manager.find(Record.class, 1L);
        manager.getTransaction().begin();
        recordDao.deleteEntity(record);
        manager.getTransaction().commit();
        totalHours = recordDao.calculateTotalHours(user, new Date(System.currentTimeMillis()));

        //total hours should be 0
        assertEquals(new Float(0), totalHours);
    }
}
