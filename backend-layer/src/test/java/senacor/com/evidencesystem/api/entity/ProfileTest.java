package senacor.com.evidencesystem.api.entity;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import senacor.com.evidencesystem.helper.TestHelper;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Profile test
 * Created by jbonk on 08.06.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProfileTest {
    private EntityManager manager;
    private TestHelper testHelper;
    private Validator validator;

    @Mock
    private Profile mockedProfile;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        validator = testHelper.initValidator();
    }

    @After
    public void closeEntityManager() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void insertProfileTest() {
        Profile profile = testHelper.createTestProfile();
        manager.getTransaction().begin();
        manager.persist(profile);
        manager.getTransaction().commit();

        assertNotNull("ID should not be null", profile.getId());
    }

    @Test
    public void deleteProfileTest() {
        Profile profile = testHelper.createTestProfile();
        manager.getTransaction().begin();
        manager.persist(profile);
        manager.getTransaction().commit();
        manager.getTransaction().begin();
        manager.remove(profile);
        manager.getTransaction().commit();

        Profile deletedProfile = manager.find(Profile.class, profile.getId());
        assertNull("Profile should be null", deletedProfile);
    }

    @Test
    public void nullProfilePropertiesTest() {
        Set<ConstraintViolation<Profile>> violations = validator.validate(mockedProfile);

        //2 violations should be present
        assertEquals(2, violations.size());
    }
}
