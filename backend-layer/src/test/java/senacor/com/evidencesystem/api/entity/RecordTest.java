package senacor.com.evidencesystem.api.entity;

import org.apache.commons.lang3.StringUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import senacor.com.evidencesystem.helper.TestHelper;

import javax.persistence.*;
import javax.validation.*;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Record test
 * Created by jbonk on 08.06.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class RecordTest {
    private EntityManager manager;
    private TestHelper testHelper;
    private Validator validator;

    @Mock
    private Record mockedRecord;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        validator = testHelper.initValidator();
    }

    @After
    public void closeEntityManager() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void insertRecordTest() {
        Record record = testHelper.createTestRecord();
        manager.getTransaction().begin();
        manager.persist(record);
        manager.getTransaction().commit();

        assertNotNull("ID should not be null", record.getId());
    }

    @Test
    public void deleteRecordTest() {
        Record record = testHelper.createTestRecord();
        manager.getTransaction().begin();
        manager.persist(record);
        manager.getTransaction().commit();
        manager.getTransaction().begin();
        manager.remove(record);
        manager.getTransaction().commit();
        Record deletedRecord = manager.find(Record.class, record.getId());

        assertNull("Record should be null", deletedRecord);
    }

    @Test
    public void nullRecordPropertiesTest() {
        Set<ConstraintViolation<Record>> violations = validator.validate(mockedRecord);

        //5 violations should be present
        assertEquals(5, violations.size());
    }

    @Test
    public void typeOfWorkSizeTest() {
        Record records = testHelper.createTestRecord();
        records.setTypeOfWork(StringUtils.EMPTY);
        Set<ConstraintViolation<Record>> violations = validator.validate(records);

        //one violation should be present
        assertEquals(1, violations.size());
        assertEquals("wrong type of work size", violations.iterator().next().getMessage());
    }
}
