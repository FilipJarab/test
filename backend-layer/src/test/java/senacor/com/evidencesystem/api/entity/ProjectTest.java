package senacor.com.evidencesystem.api.entity;

import org.apache.commons.lang3.StringUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import senacor.com.evidencesystem.helper.TestHelper;

import javax.persistence.*;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Project test
 * Created by jbonk on 08.06.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProjectTest {
    private EntityManager manager;
    private TestHelper testHelper;
    private Validator validator;

    @Mock
    private Project mockedProject;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        validator = testHelper.initValidator();
    }

    @After
    public void closeEntityManager() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void insertProjectTest() {
        Project project = testHelper.createTestProject();
        manager.getTransaction().begin();
        manager.persist(project);
        manager.getTransaction().commit();

        assertNotNull("ID should not be null", project.getId());
    }

    @Test
    public void deleteProjectTest() {
        Project project = testHelper.createTestProject();
        manager.getTransaction().begin();
        manager.persist(project);
        manager.getTransaction().commit();
        manager.getTransaction().begin();
        manager.remove(project);
        manager.getTransaction().commit();

        Project deletedProject = manager.find(Project.class, project.getId());
        assertNull("Project should be null", deletedProject);
    }

    @Test
    public void nullProjectNameTest() {
        Set<ConstraintViolation<Project>> violations = validator.validate(mockedProject);

        //one violation should be present
        assertEquals(1, violations.size());
    }

    @Test
    public void projectNameSizeTest() {
        Project project = testHelper.createTestProject();
        project.setProjectName(StringUtils.EMPTY);
        Set<ConstraintViolation<Project>> violations = validator.validate(project);

        //one violation should be present
        assertEquals(1, violations.size());
        assertEquals("wrong project name size", violations.iterator().next().getMessage());
    }
}
