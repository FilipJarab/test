package senacor.com.evidencesystem.api.entity;

import org.apache.commons.lang3.StringUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import senacor.com.evidencesystem.helper.TestHelper;

import static org.junit.Assert.*;

import javax.persistence.*;
import javax.validation.*;
import java.util.Set;

/**
 * User test
 * Created by jbonk on 26.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserTest {
    private static final String EMPTY_STRING = StringUtils.EMPTY;
    private EntityManager manager;
    private TestHelper testHelper;
    private Validator validator;

    @Mock
    private User mockedUser;

    @Before
    public void init() {
        testHelper = new TestHelper();
        manager = testHelper.initEntityManager();
        validator = testHelper.initValidator();
    }

    @After
    public void closeEntityManager() {
        if (manager != null) {
            manager.close();
        }
    }

    @Test
    public void insertUserTest() {
        User user = testHelper.createTestUser("Rr");
        manager.getTransaction().begin();
        manager.persist(user);
        manager.getTransaction().commit();
        assertNotNull("ID should not be null", user.getId());
    }

    @Test
    public void deleteUserTest() {
        User user = testHelper.createTestUser("Ss");
        manager.getTransaction().begin();
        manager.persist(user);
        manager.getTransaction().commit();
        manager.getTransaction().begin();
        manager.remove(user);
        manager.getTransaction().commit();

        User deletedUser = manager.find(User.class, user.getId());
        assertNull("User should be null", deletedUser);
    }

    @Test
    public void wrongEmailPatternTest() {
        User user = testHelper.createTestUser("Tt");
        user.setEmail("test@test.test");
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        //one violation should be present
        assertEquals(1, violations.size());
        assertEquals("wrong email pattern", violations.iterator().next().getMessage());
    }

    @Test
    public void wrongPasswordSizeTest() {
        User user = testHelper.createTestUser("Uu");
        user.setPassword(EMPTY_STRING);
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        //one violation should be present
        assertEquals(1, violations.size());
        assertEquals("wrong password size", violations.iterator().next().getMessage());
    }

    @Test
    public void wrongFirstNameSizeTest() {
        User user = testHelper.createTestUser("Vv");
        user.setFirstName(EMPTY_STRING);
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        //one violation should be present
        assertEquals(1, violations.size());
        assertEquals("wrong first name size", violations.iterator().next().getMessage());
    }

    @Test
    public void wrongLastNameSizeTest() {
        User user = testHelper.createTestUser("Ww");
        user.setLastName("");
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        //one violation should be present
        assertEquals(1, violations.size());
        assertEquals("wrong last name size", violations.iterator().next().getMessage());
    }

    @Test
    public void nullUserPropertiesTest() {
        Set<ConstraintViolation<User>> violations = validator.validate(mockedUser);

        //2 violations should be present
        assertEquals(2, violations.size());
    }
}

